// app/config/passport.js

var LocalStrategy = require("passport-local").Strategy;
var User = require("../db/models/user.js"); // user model


module.exports = function(passport) {

  // User Sessions
  // For persistent login sessions, the passport needs to be able
  // to serialize users into sessions and deserialize users out
  // of sessions.

  passport.serializeUser(function(user, done) {
    console.log("[passport serializeUser] Serializing user...")
    console.log(user);

    // Check if the user object is a promise first.
    var got = user.get(User.primaryKeyAttribute);
    if (got.then) {
      console.log("YES, the user attribute getter is a promise.");
      got.then( function(primaryKey) {
        console.log("Retrieved user, got " + primaryKey);
        if (primaryKey) {
          done(null, primaryKey);
        }
        else {
          console.log("[passport serializeUser] Error: Could not serialize user.");
          done("Unable to serialize user.");
        }
      });
    }
    else {
      console.log("NO, the user attribute getter returned the actual key: " + got);
      done(null, got);
    }
  });

  passport.deserializeUser(function(id, done) {
    console.log("DESERIALIZING " + id);
    for (foo in id) { // DELETEME
      console.log(foo);
    }
    User.findById(id).then(
      function(user) {
        if (user) {
          console.log("[passport deserializeUser] Got user object: " + user);
          console.log("[passport deserializeUser] Username parameter: " + user.get("username"));
          console.log(this.session);
          done(null, user)
        }
        else {
          done("User not found.", null);
        }
      }
    );
  });


  // local-signup Passport Strategy

  passport.use("local-signup", new LocalStrategy({
    usernameField: "username",
    passwordField: "password",
    passReqToCallback: true
  },
  function(req, username, password, done) {
    User.findOne({ where: { username: username } })
    .then(function(user) {
      if (user) {
        // The user is already registered.
        return done(null, false, "That username is already taken.");
      }
      else {
        // The user does not exist, so create one.
        var newUser = User.create({
          username: username,
          password: User.generateHash(password),
          email: "",
          chat_color: req.body.chat_color || 0x000000
        });
        return done(null, newUser, "");
      }
    });
  }
));


// local-login Passport Strategy

passport.use("local-login", new LocalStrategy({
  usernameField: "username",
  passwordField: "password",
  passReqToCallback: true
},
function(req, username, password, done) {
  User.findOne({ where: { username: username } })
  .then( function(user) {
    if (user) {
      if (User.validPassword(user, password)) {
        return done(null, user, "");
      }
      else {
        return done(null, undefined, "The password you have provided is incorrect.");
      }
    }
    else {
      return done(null, undefined, "No user found with that username.");
    }
  });
}
));

};
