// app/config/database.js

var postgres_db_config
	= 'postgres://node_dev_user:node_dev_pass@localhost:5432/condb_dev';
    // postgres://USER:PASS@DB_MACHINE:DB_PORT/DB_NAME

// Sequelize
// A node ORM that supports Postgres.
var Sequelize = require('sequelize');
var sequelize = new Sequelize(postgres_db_config);

module.exports = sequelize;