$( document ).ready(function() {
  //open first ones
  $(".game").each(function() {
    $(this).children(".phase1info").addClass("open");
  });
  $(".phase-tab:first-child").addClass("open");

  //clicking phase tabs
  $('.phase-tab').click(function(){
    //select correct phase
    var phase = $(this).parent().parent().children("."+$(this).attr("data-phase"));

    //hide all phases
    $(this).parent().parent().children(".phasearea").removeClass("open");
    $(this).parent().children(".phase-tab").removeClass("open");

    //display clicked phase
    phase.addClass("open");
    $(this).addClass("open");
  });

});