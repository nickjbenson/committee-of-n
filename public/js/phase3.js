//VARIABLES
var socket = io();
var sde_ids = [];
var socketIDs = {};
var lines = 1;



// LOADING PAGE ELEMENTS

/*function make_just(just) {
  var justlist = just.split("\n");
  for (i=1;i<=justlist.length;i++) {
      if (justlist[i-1] != "") {
          var inputline = '<div class="sioinput-wrapper"><textarea type="textarea"name="feature" class="sioinput sioinput'+i+'" placeholder="add another feature">'+justlist[i-1]+'</textarea></div>';
          $("#justifications").append(inputline);
      }
  }
  var addline = '<div class="sioinput-wrapper"><textarea name="feature" class="sioinput addline" placeholder="add another feature"></textarea></div>';
  $("#justifications").append(addline);
  lines = justlist.length+1;
}*/

function LoadSDEData() {
  $.ajax({
    type:"GET",
    url:"getSDEData",
    dataType:"json"
  }).done( function(json){
    //console.log(JSON.stringify(json));
    var sdehtml = "";
    for (catIndex in json.graycard_slots) {
      var slot_id = json.graycard_slots[catIndex].slot_id;
      var card_title = json.graycard_slots[catIndex].graycard_data.title;
      var possible_SDEs = json.graycard_slots[catIndex].possible_SDEs;
      var chosen_sde_id = json.graycard_slots[catIndex].chosen_sde_id;
      sdehtml = sdehtml + '<div class="category"><div class="category-name">'+card_title+'</div><div class="category-content">';
      for (sdeIndex in possible_SDEs) {
        var sde_id = possible_SDEs[sdeIndex].sde_id;
        var SDE_title = possible_SDEs[sdeIndex].title;
        var design_features = possible_SDEs[sdeIndex].design_features;
        var design_feature1 = design_features.split("\n")[0];
        var teacher_approved = possible_SDEs[sdeIndex].teacher_approved;
        var design_value_1 = "CHANGE";
        var design_value_2 = "CHANGE";
        var design_value_3 = "CHANGE";
        if (chosen_sde_id == sde_id) {
          sdehtml = sdehtml + '<div class="sde used" data-slotid='+slot_id+' data-sdeid='+sde_id+'>';
        } else {
          sdehtml = sdehtml + '<div class="sde" data-slotid='+slot_id+' data-sdeid='+sde_id+'>';
        }
        sdehtml = sdehtml + '<div class="sde-cards">'+design_value_1+'<br>'+design_value_2+'<br>'+design_value_3+'</div><div class="sde-content"><h3>'+SDE_title+'</h3><p>'+design_feature1+'</p><div class="sde-use"><span>Use </span><input type="checkbox" /></div></div></div>';
      }
      sdehtml = sdehtml + '</div></div>';
    }
    $("#main").append(sdehtml);
    addCheckListeners();
    //console.log(sdehtml);
  }).error( function(json){
    console.log(JSON.stringify(json));
  });
}

$(document).ready(function() { 
  LoadSDEData();
});

// CHOOSING SDES

function setSlotSDEChoice(slotid, sdeid) {
  $.ajax({
    type:"POST",
    url:"setSlotSDEChoice",
    data:{
      slot_id: slotid, // replace with slot_id from GetSDEData
      sde_id: sdeid   // replace with sde_id from GetSDEData
    },
    dataType:"json"
  }).done( function(json){
    console.log(JSON.stringify(json));
  }).error( function(json){
    console.log(JSON.stringify(json));
  });
}

function addCheckListeners() {
  /*$('input:checkbox').change(function(){
    if ($(this).is(':checked')) {
      var sdeObj = $(this).parents(".sde");
      var categoryObj = $(this).parents(".category");
      //$(categoryObj).find("input:checkbox").attr('checked', false);
      $(this).attr('checked', true);
      setSlotSDEChoice($(sdeObj).attr('data-slotid'), $(sdeObj).attr('data-sdeid'));
    }
  });*/
}

/* phase 2 stuff
function make_text_lines(features, setIndex) {
  var featurelist = features.split("\n");
  for (i=1;i<=featurelist.length;i++) {
    if (featurelist[i-1] != "") {
      console.log(featurelist[i-1]);
      var textline = '<div class="form-info-wrapper"><p>'+featurelist[i-1]+'</p></div>'
      $("#set"+setIndex).find(".features").append(textline);
    }
  }
}

function createSDE(json) {
  console.log(JSON.stringify(json));
  sorteddata = json.phase2hands.sort(function(a, b) {
    return ((a.sde_id > b.sde_id) ? 1 : -1 );
  });
  // TODO change this this is for the online section for phase 1 
  var statusNum = json.phaseApprovedByTeacher;
  if (statusNum == true) {
    var status = "This set has been approved, and editing is locked.";
    var symbolHTML = "<li><i class='fa fa-check'></i></li>";
  } else if (statusNum == false) {
    var status = "Draft";
    var symbolHTML = "<li><i class='fa fa-hourglass'></i></li>";
  } else {
    var status = "Sent Back for Revision";
    var symbolHTML = "<li><i class='fa fa-close'></i></li>";
  }
  $("ul.approvals").append(symbolHTML);
  $("#status").text(status);
  
  if (userIsTeacherForSession) {
    $("#online-container ul.users").children("li").each(function(index) {
      if ($(this).text() == sessionUsername) {
        $("#online-container ul.approvals li:nth-child("+(index+1)).html(symbolHTML);
      }
    });
  }
  
  for (j in sorteddata) {
    var setdata = sorteddata[j];
    sde_ids.push(setdata.sde_id);
    var setNum = parseInt(j)+1;

    if (setdata.chosen_for_school) {
      var chosenForSchool = " chosen";
    } else {
      var chosenForSchool = "";
    }

    // make setlink
    $("#sets").append('<p class="setlink'+chosenForSchool+'" id="setlink'+setNum+'" data-id="'+setNum+'">'+setNum+'</p>');
    

    var sde = '<div class="set" id="set'+setNum+'"><h1 class="design-title">'+setdata.title+'</h1><div class="hand-cards"><div class="design-card" id="design-element"><div class="design-card-container"><h1>'+setdata.design_element_card.title+'</h1><img class="embellishment" src="/assets/embellishment.png" /><p>“'+setdata.design_element_card.quote_text+'”</p><p>- '+setdata.design_element_card.quote_author+'</p><img class="embellishment" src="/assets/embellishment.png" /></div></div>'; // area for design element
    var sde = sde + '<div class="design-card" id="design-value-1"><div class="design-card-container"><h1>'+setdata.design_value_1.title+'</h1><img class="embellishment" src="/assets/embellishment.png" /><p>“'+setdata.design_value_1.quote_text+'”</p><p>- '+setdata.design_value_1.quote_author+'</p><img class="embellishment" src="/assets/embellishment.png" /></div></div>'; // area for design value 1
    var sde = sde + '<div class="design-card" id="design-value-2"><div class="design-card-container"><h1>'+setdata.design_value_2.title+'</h1><img class="embellishment" src="/assets/embellishment.png" /><p>“'+setdata.design_value_2.quote_text+'”</p><p>- '+setdata.design_value_2.quote_author+'</p><img class="embellishment" src="/assets/embellishment.png" /></div></div>'; // area for design value 2
    var sde = sde + '<div class="design-card" id="design-value-3"><div class="design-card-container"><h1>'+setdata.design_value_3.title+'</h1><img class="embellishment" src="/assets/embellishment.png" /><p>“'+setdata.design_value_3.quote_text+'”</p><p>- '+setdata.design_value_3.quote_author+'</p><img class="embellishment" src="/assets/embellishment.png" /></div></div></div>'; // area for design value 3
    var sde = sde + '<div class="form"><h2>Key Design Features</h2><div class="form-section features"><div class="form-info-wrapper"><p></p></div></div><button class="choose'+chosenForSchool+'">Choose</button></div></div>'; // form

    $("#main").append(sde);

    make_text_lines(setdata.design_features, j);
  }

  //open first SDE
  $('#set1').addClass("open");
  $('#setlink1').addClass("setSelected");

  //clicking an SDE number
  $('.setlink').click(function(){
    //selectset
    var set = $("#set"+$(this).attr("data-id"));
    console.log(set);
    //hide all sets
    $(".set").removeClass("open");
    $(".setlink").removeClass("setSelected");
    //display clicked card
    set.addClass("open");
    $(this).addClass("setSelected");
  });

  //a user clicks choose
  $(".choose").click(function() {
    var setIndex = $(this).parents(".set").attr("id").slice(-1);
    var sdeid = sde_ids[parseInt(setIndex) - 1];
    if($(this).hasClass("chosen")) {
      var chosen = false;
    } else {
      var chosen = true;
    }
    
    $.ajax({
      type: "POST",
      url: "sdeChoice",
      data: {
        sde_id: sdeid,
        chosen_for_school: chosen
      },
      dataType:"json"
    }).done( function(json){
      //console.log(JSON.stringify(json));
      socket.emit('updatesetlink', setIndex);
    });
  });
}

function loadHandViewData() {
  $.ajax({
    type: "GET",
    url: "handdata",
    dataType: "json"
  }).done( function(json) {
    createSDE(json);
  }).error( function(json) {
    console.error("[loadHandViewData] error:");
    console.error(json);
  });
}

// this loads the user approval data
$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "getPhaseApprovals",
    dataType: "json"
  }).done( function(json){
    console.log(JSON.stringify(json));
    var sortedApprovals = json.userApprovals.sort(function(a, b) {
        return ((a.username > b.username) ? 1 : -1 );
    });
    console.log(JSON.stringify(sortedApprovals));
    //start user_html
    var users_html = "<ul class='users'><li class='heading'>Users</li>";

    //populate users
    for (userIndex in sortedApprovals) {
        var username = sortedApprovals[userIndex].username;
        var online = sortedApprovals[userIndex].is_user_online;
        if (online) {
            users_html = users_html + "<li class='user online'>" + username + "</li>"; 
        } else {
            users_html = users_html + "<li class='user'>" + username + "</li>"; 
        }
        
    }
    users_html = users_html + "<li class='user'>Teacher</li></ul>";

    //populate approvals
    users_html = users_html + "<ul class='approvals'><li class='heading'>Approved</li>";
    for (userIndex in sortedApprovals) {
        var approved = sortedApprovals[userIndex].approved;
        if (approved == 1) {
          users_html = users_html + "<li><i class='fa fa-check'></i></li>";
        } else {
          users_html = users_html + "<li><i class='fa fa-close'></i></li>";
        }
        
    }
    users_html = users_html + "</ul>";
    console.log(users_html);

    //append to online container
    $("#online-container").append(users_html);

    //load teacher approvals and hand data (needs to be after online section populated)
    loadHandViewData();

  });
});

// CHAT 

function format_time(time) {
  var re = /([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{1,3}):([0-9]{1,3}):([0-9]{1,4})/;
  var time_elements = re.exec(time);
  var year = time_elements[1];
  var month = time_elements[2];
  var day = time_elements[3];
  var hour = time_elements[4];
  var minute = time_elements[5];
  var formatted = month+'/'+day+" "+hour+":"+minute;
  return formatted;
}

function updateChatScroll(){
  var element = document.getElementById("chat-container");
  element.scrollTop = element.scrollHeight;
}

$(document).ready( function() {
  $.ajax({
    url: "getChatHistory",
    data: {},
    type: "GET",
    dataType: "json",
  }).done(function(json) {
    ch = json;
    for (i=ch.length-1;i>=0;i--) {
      var chatmessage = '<div class="chat-message"><div class="chat-time">'+format_time(ch[i]["createdAt"])+'</div><div class="chat-content"><span class="chat-author chat-author-alice">'+ch[i]["username"]+': </span>'+ch[i]["message"]+'</div></div>';
        $("#chat-message-area").append(chatmessage);
    }
    updateChatScroll();

  });

  // set online for the user that loaded the page
  socket.emit('newonline', sessionUsername);
  $.ajax({
    type: "POST",
    url: "setUserOnline",
    data: true,
    dataType: "JSON"
  }).done(function(json) {
    console.log("Got back setApproval response: " + JSON.stringify(json));
  });

});


// Chat window
$("textarea#chat-new").elastic();

function pushChatMessage(chat_text) {
  console.log("sending chat text: " + chat_text);
  $.ajax({
      url: "pushChatMessage",
      data: {
          message: chat_text,
      },
      type: "POST",
      dataType: "json",
  }).done( function(json) {
      if (json.error !== undefined) {
          console.error("[pushChatMessage] failed: " + JSON.stringify(json));
      } else {
          console.log("[pushChatMessage] succeeded! " + JSON.stringify(json));
      }
  });
}
$('#chat-new').keypress(function (e) {
  if (e.which == 13 && $('#chat-new').val() != "") {
      var chat_text = $('#chat-new').val();
      socket.emit('newmessage', chat_text, sessionUsername);
      pushChatMessage(chat_text);
      $('#chat-new').val("");
      return false;
  }
});
socket.on('newmessage', function(content, username){
  var date = new Date();
  var dateformatted = format_time(date.toISOString());
  var chatmessage = '<div class="chat-message"><div class="chat-time">'+dateformatted+'</div><div class="chat-content"><span class="chat-author chat-author-alice">'+username+': </span>'+content+'</div></div>';
  $("#chat-message-area").append(chatmessage);
  updateChatScroll();
});


// approve button 
$("#approve-button").click(function() {
  $.ajax({
    type: "POST",
    url: "setPhaseApproval",
    data: {
      approved: 1
    },
    dataType:"json"
  }).done(function(json) {
    console.log("Got back setApproval response: " + JSON.stringify(json));
    $("#approve-button").addClass("green").delay(1000).queue(function() {
        $("#approve-button").removeClass("green");
        $("#approve-button").dequeue();
    });
    if (userIsTeacherForSession) {
        socket.emit('updatestatus', "", 'Teacher', "check");
    } else {
        socket.emit('updatestatus', "", sessionUsername, "check");
    }
  });
});

if (!userIsTeacherForSession) {
  $("reject-button").hide();
}
// Reject button
$("#reject-button").click(function() {
  $.ajax({
    type: "POST",
    url: "setPhaseApproval",
    data: {
      approved: -1
    },
    dataType:"json"
  }).done(function(json) {
    console.log("Got back setApproval response: " + JSON.stringify(json));
    $("#reject-button").addClass("red").delay(1000).queue(function() {
        $("#reject-button").removeClass("red");
        $("#reject-button").dequeue();
    });
    if (userIsTeacherForSession) {
        socket.emit('updatestatus', "", 'Teacher', "close");
    } else {
        socket.emit('updatestatus', "", sessionUsername, "close");
    }
  });
});

//update setlink
socket.on('updatesetlink', function(setIndex){
  var setObject = $("#set"+setIndex);
  if($(setObject).find("button.choose").hasClass("chosen")) {
    $(setObject).find("button.choose").removeClass("chosen");
    $(setObject).find("button.choose").text("Choose");
    $("#setlink"+setIndex).removeClass("chosen");
    var chosen = false;
  } else {
    $(setObject).find("button.choose").addClass("chosen");
    $(setObject).find("button.choose").text("Chosen");
    $("#setlink"+setIndex).addClass("chosen");
    var chosen = true;
  }
});

//status stuff
socket.on('updatestatus', function(change_status, username, status){
  $("#online-container ul.users").children("li").each(function(index) {
      if ($(this).text() == username) {
          $("#online-container ul.approvals li:nth-child("+(index+1)).html("<i class='fa fa-"+status+"'></i>");
      }
  });
  if (userIsTeacherForSession) {
      $("#online-container ul.users").children("li").each(function(index) {
          if ($(this).text() == sessionUsername) {
              $("#online-container ul.approvals li:nth-child("+(index+1)).html("<i class='fa fa-"+status+"'></i>");
          }
      });
  }
  if (username == "Teacher" && status == "check") {
      $("#status").text("This set has been approved, and editing is locked.");
  }
});

socket.on('newonline', function(username, socketID){
  $("#online-container ul.users").children("li").each(function(index) {
      if ($(this).text() == username) {
          $(this).addClass("online");
      }
  });
  socketIDs[socketID] = username;
});

//implement disconnect
socket.on('newdisconnect', function(socketID){
  username = socketIDs[socketID];
  $("#online-container ul.users").children("li").each(function(index) {
      if ($(this).text() == username) {
          $(this).removeClass("online");
      }
  });
});


*/
