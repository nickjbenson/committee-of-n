$( document ).ready(function() {
  //click the caret
  $('.groupcaret').click(function(){
    //if the form items are hidden
    if ($(this).parent().children(".groupforms").hasClass("hidden")) {
      //expand the form items
      $(this).parent().children(".groupforms").removeClass("hidden");
      //rotate the caret so its now pointing up
      $(this).parent().find(".fa-caret-down").addClass("up");
    } else {
      //hide the form items
      $(this).parent().children(".groupforms").addClass("hidden");
      //rotate the caret so its now pointing down
      $(this).parent().find(".fa-caret-down").removeClass("up");
    }

  });
});