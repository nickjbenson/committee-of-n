$( document ).ready(function() {
  //open first one
  $("#design-element").addClass("open");

  //clicking design tabs
  $('.design-tab').click(function(){
    //select correct card
    var card = $("#"+$(this).attr("data-card"));

    //hide all cards
    $(".design-area").removeClass("open");

    //display clicked card
    card.addClass("open");
  });

  /*resize text 
  jQuery(".sioinput").on("keydown keyup", function(){
    this.style.height = "1px";
    this.style.height = (this.scrollHeight) + "px"; 
  });*/
});