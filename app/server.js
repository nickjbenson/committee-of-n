// app/server.js


// Modules

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var passport = require("passport"); // authentication
var flash = require("connect-flash"); // simple session messages
var session = require("express-session"); // remember a user session
var morgan = require("morgan"); // Dev: Log requests
require('sequelize');
require('../config/database.js');


// Setup

// Statically serve the public folder (for CSS / HTML / JS front-end)
var path = require("path");
app.use(express.static(path.join(__dirname, "/../public"))); // statically serve the public folder

// Use EJS as the default view engine.
app.set("view engine", "ejs");
var helpers = require("express-helpers")(app); // EJS helpers -- "link_to()"

// SocketIO middleware
var http = require("http").Server(app);
var io = require("socket.io")(http);
io.on("connection", function(socket){
  socket.on("addline", function(setIndex){
    io.emit("addline", setIndex);
  });
  socket.on("removeempty", function(set, classnum){
    io.emit("removeempty", set, classnum);
  });
  socket.on("keyup", function(content,set, classnum){
    socket.broadcast.emit("keyup", content, set, classnum);
    socket.broadcast.emit("lock", set, classnum);
    //io.emit("resizearea", content, set, classnum);
  });
  socket.on("lock", function(set, classnum){
    socket.broadcast.emit("lock", set, classnum);
  });
  socket.on("unlock", function(set, classnum){
    socket.broadcast.emit("unlock", set, classnum);
    io.emit("removeempty", set, classnum);
  });
  //Chat stuff
  socket.on("newmessage", function(content, username){
    io.emit("newmessage", content, username);
  });
  //Update setlink
  socket.on("updatesetlink", function(change_status, username){
    io.emit("updatesetlink", change_status, username);
  });
  //Status stuff
  socket.on("updatestatus", function(change_status, username, status){
    io.emit("updatestatus", change_status, username, status);
  });
  socket.on('newonline', function(username){
    //onlineUsers[username] = true;
    //socketIDs[socket.id] = username;
    io.emit("newonline", username, socket.id);
  });
  socket.on('disconnect', function(){
    //username = socketIDs[socket.id];
    //onlineUsers[username] = false;
    io.emit("newdisconnect", socket.id);
  });
});

// The rest of the middleware stack
app.use(bodyParser.urlencoded({ extended: true }) );
app.use(session({ secret: "super_secret_con_dev" }));
app.use(function(req, res, next) {
  res.locals.messages = req.session.messages
  next()
});
require("../config/passport.js")(passport); // configure passport with passport.js
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(morgan("dev")); // Dev: Log requests to the console


// Cards
// Require base.js so we have the base game deck in the DB.
require("../db/cards/base.js");


// Routes

require("../app/routes/main.js")(app, passport);


// Launch

http.listen(3000, function ()  {
	console.log("Example application is listening on port 3000!")
});
