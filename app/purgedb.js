// app/purgedb.js
// As it says on the tin, running this will destroy everything in the database.
// Stay in school, kids!
var Sequelize = require("sequelize");
var sequelize = require("../config/database.js");

var GameSession = require("../db/models/gamesession.js");
var User = require("../db/models/user.js");
var StudentTeam = require("../db/models/studentteam.js");
var UserXStudentTeam = require("../db/models/userxstudentteam.js");
var TeacherPrivelege = require("../db/models/teacherprivelege.js");
var SchoolDesignElement = require("../db/models/schooldesignelement.js");
var Card = require("../db/models/card.js");
var CardType = require("../db/models/cardtype.js");
var ChatSession = require("../db/models/chatsession.js");
var UserXChatSession = require("../db/models/userxchatsession.js");
var PhaseInstance = require("../db/models/phaseinstance.js");
var ChatMessage = require("../db/models/chatmessage.js");
var Phase2Choice = require("../db/models/phase2choice.js");
var SDEApproval = require("../db/models/sdeapproval.js");
var Phase3MergedMembership = require("../db/models/phase3mergedmembership.js");
var Phase3Slot = require("../db/models/phase3slot.js");
var Phase3Choice = require("../db/models/phase3choice.js");

sequelize.sync({ force: true });
console.log("OK, it's all gone.");
