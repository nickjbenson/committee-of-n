// app/query.js
// Implements methods that return Promises to return data from the DB.

var GameSession = require("../db/models/gamesession.js");
var User = require("../db/models/user.js");
var StudentTeam = require("../db/models/studentteam.js");
var UserXStudentTeam = require("../db/models/userxstudentteam.js");
var TeacherPrivelege = require("../db/models/teacherprivelege.js");
var SchoolDesignElement = require("../db/models/schooldesignelement.js");
var Card = require("../db/models/card.js");
var CardType = require("../db/models/cardtype.js");
var ChatSession = require("../db/models/chatsession.js");
var UserXChatSession = require("../db/models/userxchatsession.js");
var PhaseInstance = require("../db/models/phaseinstance.js");
var ChatMessage = require("../db/models/chatmessage.js");
var Phase2Choice = require("../db/models/phase2choice.js");
var SDEApproval = require("../db/models/sdeapproval.js");
var Phase3MergedMembership = require("../db/models/phase3mergedmembership.js");
var Phase3Slot = require("../db/models/phase3slot.js");
var Phase3Choice = require("../db/models/phase3choice.js");
require('sequelize');
require('../config/database.js');

var exportFunctions = {};


function GetColumnsFromInstances(instances, columnName) {
  var columns = [];
  for (var i = 0; i < instances.length; i++) {
    columns.push(instances[i][columnName]);
  }
  return columns;
}

function IDsFromInstances(instances, Model) {
  return GetColumnsFromInstances(instances, Model.primaryKeyAttribute);
}

function InstancesFromIDsPromise(ids, Model) {
  var whereObj = {}
  whereObj[Model.primaryKeyAttribute] = ids
  return Model.findAll({
    where: whereObj
  }).then (function(instances) {
    return instances;
  });
}

// Sometimes it may be easier for higher-level code to provide an instance
// instead of an instance ID; this function checks for that and returns an ID
// always. This prevents higher-level code from having to know, e.g., what
// the primary key attribute for a given Model is (which would mean having a
// require("path/to/model/definition.js") dependency for that instance Model).
function MaybeIDFromInstance(IDorInstance) {
  if (IDorInstance["id"] !== undefined) {
    return IDorInstance["id"];
  }
  else {
    return IDorInstance;
  }
}

function MaybeIDsFromInstances(IDsOrInstances) {
  for (var i = 0; i < IDsOrInstances.length; i++) {
    IDsOrInstances[i] = MaybeIDFromInstance(IDsOrInstances[i]);
  }
  return IDsOrInstances;
}

function MaybeInstanceFromIDPromise(IDorInstance, Model) {
  if (typeof IDorInstance == "number") {
    return Model.findById(IDorInstance);
  }
  else return new Promise( function(resolve, reject) {
    resolve(IDorInstance);
  });
}

function BlankArray(numSlots) {
  var arr = [];
  for (var i = 0; i < numSlots; i++) {
    arr.push({});
  }
  return arr;
}

function RunPromiseFunctionWithReturnArg(promiseFunc, arg, extraArg, returnArg) {
  return new Promise( function(resolve, reject) {
    promiseFunc(arg, extraArg).then( function(funcResponse) {
      resolve({
        funcResponse: funcResponse,
        returnArg: returnArg
      });
    });
  });
}

function RunPromiseFunctionWithTwoReturnArgs(promiseFunc, arg, extraArg, returnArg1, returnArg2) {
  return new Promise( function(resolve, reject) {
    promiseFunc(arg, extraArg).then( function(funcResponse) {
      resolve({
        funcResponse: funcResponse,
        returnArg1: returnArg1,
        returnArg2: returnArg2
      });
    });
  });
}

// Maps a Promise-returning function that takes one argument and an optional extra argument
// into an array of that function's responses, preserving order.
function MapPromise(promiseFunc, arrayLike, extraArg) {
  return new Promise( function(resolve, reject) {
    if (arrayLike.length == 0) {
      resolve([]);
    }
    else {
      var responseArray = BlankArray(arrayLike.length);
      var counter = 0;
      for (var i = 0; i < arrayLike.length; i++) {
        RunPromiseFunctionWithReturnArg(promiseFunc, arrayLike[i], extraArg, i).then( function(responseAndReturnArg) {
          var mapFuncResponse = responseAndReturnArg["funcResponse"];
          var index = responseAndReturnArg["returnArg"];
          responseArray[index] = mapFuncResponse;
          counter++;
          if (counter == arrayLike.length) {
            resolve(responseArray);
          }
        });
      }
    }
  });
}
exportFunctions["MapPromise"] = MapPromise;

// Maps a Promise-returning function that takes two arguments into an array of that
// function's responses by using the arrayLike1 and arrayLike2 arguments in tandem and in order.
function DoubleMapPromise(promiseFunc, arrayLike1, arrayLike2) {
  return new Promise( function(resolve, reject) {
    if (arrayLike1.length == 0 || arrayLike2.length == 0) {
      resolve([]);
    }
    else if (arrayLike1.length != arrayLike2.length) {
      console.error("[DoubleMapPromise] First array length " + arrayLike1.length + " does not match second array length " + arrayLike2.length + ", returning empty list.");
      resolve([]);
    }
    else {
      var responseArray = BlankArray(arrayLike1.length);
      var counter = 0;
      for (var i = 0; i < arrayLike1.length; i++) {
        RunPromiseFunctionWithReturnArg(promiseFunc, arrayLike1[i], arrayLike2[i], i).then( function(responseAndReturnArg) {
          var mapFuncResponse = responseAndReturnArg["funcResponse"];
          var index = responseAndReturnArg["returnArg"];
          responseArray[index] = mapFuncResponse;
          counter++;
          if (counter == arrayLike1.length) {
            resolve(responseArray);
          }
        });
      }
    }
  });
}
exportFunctions["DoubleMapPromise"] = DoubleMapPromise;

// Maps a Promise-returning function that takes two arguments into a flat list
// of that function run for all possible combinations of the input argument
// arguments as pairs with one from either list.
// e.g. running with myFunction, [1, 2], [3, 4] will return a flat list:
// [myFunction(1, 3), myFunction(1, 4), myFunction(2, 3), myFunction(2, 4)]
function CrossMapPromise(promiseFunc, arrayLike1, arrayLike2) {
  return new Promise( function(resolve, reject) {
    if (arrayLike1.length == 0 || arrayLike2.length == 0) {
      resolve([]);
    }
    else {
      var responseArray = BlankArray(arrayLike1.length * arrayLike2.length);
      var counter = 0;
      for (var i = 0; i < arrayLike1.length; i++) {
        for (var j = 0; j < arrayLike2.length; j++) {
          RunPromiseFunctionWithTwoReturnArgs(promiseFunc, arrayLike1[i], arrayLike2[j], i, j).then( function(responseAndReturnArgs) {
            var mapFuncResponse = responseAndReturnArgs["funcResponse"];
            var indexI = responseAndReturnArgs["returnArg1"];
            var indexJ = responseAndReturnArgs["returnArg2"];
            responseArray[indexI + arrayLike1.length * indexJ] = mapFuncResponse;
            counter++;
            if (counter == arrayLike1.length * arrayLike2.length) {
              resolve(responseArray);
            }
          });
        }
      }
    }
  });
}
exportFunctions["CrossMapPromise"] = CrossMapPromise;

function MapPromiseWithReturnArg(promiseFunc, instances, extraArg, returnArg) {
  return new Promise( function(resolve, reject) {
    MapPromise(promiseFunc, instances, extraArg).then( function(responseArray) {
      resolve({
        result: responseArray,
        returnArg: returnArg
      });
    });
  });
}

function GameSessionsFromStudentTeams(studentteams) {
  var gamesession_ids = GetColumnsFromInstances(studentteams, "gamesession_id");
  return InstancesFromIDsPromise(gamesession_ids, GameSession);
}

function GetGameSessionsFromUserXStudentTeams(userxstudentteams) {
  var studentteam_ids = GetColumnsFromInstances(userxstudentteams, "studentteam_id");
  return InstancesFromIDsPromise(studentteam_ids, StudentTeam).then( function(studentteams) {
    return GameSessionsFromStudentTeams(studentteams);
  });
}

function GetTeacherPriveleges(user_id) {
  user_id = MaybeIDFromInstance(user_id);
  return TeacherPrivelege.findAll({
    where: { user_id: user_id }
  });
}

function GetGameSessionIDsTeacherOnly(user_id, gamesessionsParticipating) {
  user_id = MaybeIDFromInstance(user_id);
  return GetTeacherPriveleges(user_id).then( function(teacherPriveleges) {
    var gamesessionsTeacherPrivelegeIDs = GetColumnsFromInstances(teacherPriveleges, "gamesession_id");
    var gamesessionsTeacherOnlyIDs = []
    for (var i = 0; i < gamesessionsTeacherPrivelegeIDs.length; i++) {
      var unique = true;
      for (var j = 0; j < gamesessionsParticipating.length; j++) {
        if (MaybeIDFromInstance(gamesessionsParticipating[j]) == gamesessionsTeacherPrivelegeIDs[i]) {
          unique = false;
          break;
        }
      }
      if (unique) {
        gamesessionsTeacherOnlyIDs.push(gamesessionsTeacherPrivelegeIDs[i]);
      }
    }
    console.log("[GetGameSessionIDsTeacherOnly] gamesessionsTeacherOnlyIDs " + JSON.stringify(gamesessionsTeacherOnlyIDs));
    return gamesessionsTeacherOnlyIDs;
  });
}

function GetPhase3TeamsFromStudentTeams(studentteams) {
  var studentteam_ids = MaybeIDsFromInstances(studentteams);
  return Phase3MergedMembership.findAll({
    where: {
      studentteam_id: studentteam_ids
    }
  }).then( function(phase3mergedmemberships) {
    console.log("[GetPhase3TeamsFromStudentTeams] got phase3mergedmemberships: " + JSON.stringify(phase3mergedmemberships));
    var phase3TeamIDsEntered = {};
    var uniquePhase3TeamIDs = [];
    var phase3TeamIDs = GetColumnsFromInstances(phase3mergedmemberships, "phase3team_id");
    for (var i = 0; i < phase3TeamIDs.length; i++) {
      var phase3TeamID = phase3TeamIDs[i];
      if (phase3TeamIDsEntered[phase3TeamID] === undefined) {
        uniquePhase3TeamIDs.push(phase3TeamID);
        phase3TeamIDsEntered[phase3TeamID] = true;
      }
    }
    return InstancesFromIDsPromise(uniquePhase3TeamIDs, StudentTeam);
  });
}

function GetUsersInPhase3Team(phase3team_id) {
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return Phase3MergedMembership.findAll({
    where: {
      phase3team_id: phase3team_id
    }
  }).then( function(phase3mergedmemberships) {
    return MapPromise(
      GetUsersInTeam,
      GetColumnsFromInstances(phase3mergedmemberships, "studentteam_id")
    ).then( function(usersArrays) {
      var flatUsers = [];
      for (var i = 0; i < usersArrays.length; i++) {
        for (var j = 0; j < usersArrays[i].length; j++) {
          flatUsers.push(usersArrays[i][j]);
        }
      }
      return flatUsers;
    });
  });
}

function CheckApprovedByUsersInTeam(SDE, studentteam_id) {
  return GetUsersInTeam(studentteam_id).then( function(users) {
    return MapPromise(
      GetOrCreateSDEApprovalForUserandSDE,
      users,
      SDE
    ).then( function(sdeApprovals) {
      for (var i = 0; i < sdeApprovals.length; i++) {
        if (sdeApprovals[i]["approved"] == 0) {
          return false;
        }
      }
      return true;
    });
  });
}

function CountSubmittedSDEsForStudentTeam(SDEs, studentteam_id) {
  return MapPromise(
    CheckApprovedByUsersInTeam,
    SDEs,
    studentteam_id
  ).then( function(booleans) {
    var countTrues = 0;
    for (var i = 0; i < booleans.length; i++) {
      if (booleans[i]) {
        countTrues += 1;
      }
    }
    return countTrues;
  });
}

function GetPhase1SDESummaryData(studentteam_id) {
  return new Promise( function(resolve, reject) {
    var data = {
      SDEsStarted: 0,
      SDEsSubmitted: 0,
      SDEsApproved: 0
    };
    GetSchoolDesignElementsForStudentTeam(studentteam_id).then( function(SDEs) {
      for (var i = 0; i < SDEs.length; i++) {
        var SDE = SDEs[i];
        if (SDE["design_features"].length > 0) {
          data.SDEsStarted += 1;
        }
        if (SDE["teacher_approved"]) {
          data.SDEsApproved += 1;
        }
      }
      CountSubmittedSDEsForStudentTeam(SDEs, studentteam_id).then( function(numApprovedSDEs) {
        data.SDEsSubmitted = numApprovedSDEs;
        resolve(data);
      });
    });
  });
}

function GetPhase2SDESummaryData(studentteam_id) {
  return new Promise( function(resolve, reject) {
    var data = {
      SDEsChosen: 0,
      SDEsAvailable: 0,
      teacherApproved: "No",
    }
    GetPhase2ChoicesForStudentTeam(studentteam_id).then( function(phase2Choices) {
      data.SDEsAvailable = phase2Choices.length;
      for (var i = 0; i < phase2Choices.length; i++) {
        if (phase2Choices[i]["chosen_for_school"]) {
          data.SDEsChosen += 1;
        }
      }
      GetPhaseInstanceForTeamAndPhase(studentteam_id, 2).then( function(phaseinstance) {
        if (phaseinstance["teacher_approved"]) {
          data.teacherApproved = "Yes";
        }
        resolve(data);
      });
    });
  });
}

function GetPhase3SDESummaryData(phase3team_id) {
  return new Promise( function(resolve, reject) {
    var data = {
      SDEsAvailable: 0,
      SDESlotsAvailable: 0,
      SDESlotsChosen: 0,
      teacherApproved: "No"
    };
    GetPhase3SDEDataForTeam(phase3team_id).then( function(phase3SDEData) {
      for (var i = 0; i < phase3SDEData.graycard_slots.length; i++) {
        if (phase3SDEData.graycard_slots[i].chosen_sde_id != 0) {
          data.SDESlotsChosen += 1;
        }
        data.SDEsAvailable += phase3SDEData.graycard_slots[i].possible_SDEs.length;
        if (phase3SDEData.graycard_slots[i].possible_SDEs.length > 0) {
          data.SDESlotsAvailable += 1;
        }
      }
      GetPhaseInstanceForTeamAndPhase(phase3team_id, 3).then( function(phaseinstance) {
        if (phaseinstance["teacher_approved"] == 1) {
          data.teacherApproved = "Yes";
        }
        resolve(data);
      });
    });
  });
}

function GetPhase3TeamData(phase3team_id) {
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return GetUsersInPhase3Team(phase3team_id).then( function(users) {
    console.log("[GetPhase3TeamData] got users: " + JSON.stringify(users));
    return GetPhase3SDESummaryData(phase3team_id).then( function(phase3SDESummaryData) {
      return {
        phase3TeamID: phase3team_id,
        usersInPhase3Team: GetColumnsFromInstances(users, "username"),
        SDEsAvailable: phase3SDESummaryData.SDEsAvailable,
        SDESlotsAvailable: phase3SDESummaryData.SDESlotsAvailable,
        SDESlotsChosen: phase3SDESummaryData.SDESlotsChosen,
        readyForTeacher: phase3SDESummaryData.readyForTeacher,
        teacherApproved: phase3SDESummaryData.teacherApproved
      };
    });
  });
}

function GetPhaseNTeamData(studentteam_id, phase) {
  if (phase == 3) {
    // phase 3 data can'y rely on UserXStudentTeam records; phase 3 teams are merged from earlier-phase teams.
    console.error("[GetPhaseNTeamData] Won't work for phase 3, which uses special student teams. Returning null.");
    return null;
  }
  else {
    studentteam_id = MaybeIDFromInstance(studentteam_id);
    return UserXStudentTeam.findAll({
      where: {
        studentteam_id: studentteam_id
      }
    }).then( function(userxstudentteams) {
      if (userxstudentteams == null) {
        console.log("[GetPhaseNTeamData] Couldn't get userxstudentteam records for studentteam_id: " + studentteam_id);
        return {};
      }
      else {
        return InstancesFromIDsPromise(
          GetColumnsFromInstances(userxstudentteams, "user_id"),
          User
        ).then( function(users) {
          var usersInTeam = GetColumnsFromInstances(users, "username");
          if (phase == 1) {
            return GetPhase1SDESummaryData(studentteam_id).then( function(phase1SDESummaryData) {
              var responseObj = {
                studentTeamID: studentteam_id,
                usersInTeam: usersInTeam,
                SDEsStarted: phase1SDESummaryData.SDEsStarted,
                SDEsSubmitted: phase1SDESummaryData.SDEsSubmitted,
                SDEsApproved: phase1SDESummaryData.SDEsApproved
              };
              return responseObj;
            });
          }
          else { // phase == 2
            return GetPhase2SDESummaryData(studentteam_id).then( function(phase2SDESummaryData) {
              var responseObj = {
                studentTeamID: studentteam_id,
                usersInTeam: usersInTeam,
                SDEsChosen: phase2SDESummaryData.SDEsChosen,
                SDEsAvailable: phase2SDESummaryData.SDEsAvailable,
                readyForTeacher: phase2SDESummaryData.readyForTeacher,
                teacherApproved: phase2SDESummaryData.teacherApproved
              };
              return responseObj;
            });
          }
        });
      }
    });
  }
}

function GetStudentTeamsForSession(gamesession_id) {
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return StudentTeam.findAll({
    where: {
      gamesession_id: gamesession_id,
      is_phase3_team: false
    }
  }).then( function(studentTeams) {
    return studentTeams;
  });
}

function GetStudentTeamForUserInGameSession(user_id, gamesession_id) {
  user_id = MaybeIDFromInstance(user_id);
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return UserXStudentTeam.findAll({
    where: {
      user_id: user_id
    }
  }).then( function(userxstudentteams) {
    var studentteam_ids = GetColumnsFromInstances(userxstudentteams, "studentteam_id");
    return InstancesFromIDsPromise(studentteam_ids, StudentTeam).then( function(studentteams) {
      for (var i = 0; i < studentteams.length; i++) {
        if (studentteams[i].get("gamesession_id") == gamesession_id) {
          return studentteams[i];
        }
      }
      console.error("[GetStudentTeamForUserInGameSession] Never finished building studentteams; returning null");
      return null;
    });
  });
}
exportFunctions["GetStudentTeamForUserInGameSession"] = GetStudentTeamForUserInGameSession;

function GetStudentTeamsMaybeFilteredByUser(doFilter, user_id, gamesession_id) {
  user_id = MaybeIDFromInstance(user_id);
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  if (doFilter) {
    return GetStudentTeamForUserInGameSession(user_id, gamesession_id).then( function(studentTeam) {
      return [studentTeam];
    });
  }
  else {
    return GetStudentTeamsForSession(gamesession_id);
  }
}

function GetTeacherUsernamesFromGameSession(gamesession_id) {
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return TeacherPrivelege.findAll({ where: { gamesession_id: gamesession_id }}).then( function(teacherpriveleges) {
    return User.findAll({ where: { id: GetColumnsFromInstances(teacherpriveleges, "user_id") }}).then( function(users) {
      return GetColumnsFromInstances(users, "username");
    });
  });
}

function GetUsersInTeam(studentteam_id) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return UserXStudentTeam.findAll({ where: { studentteam_id: studentteam_id }}).then( function(userxstudentteams) {
    //console.log("[GetUsersInTeam] userxstudentteams for studentteam_id " + studentteam_id + ": " + JSON.stringify(userxstudentteams));
    return InstancesFromIDsPromise(GetColumnsFromInstances(userxstudentteams, "user_id"), User).then( function(users) {
      return users;
    });
  });
}

function GetOverviewDataFromGameSessionParticipatingAndUser(gamesessionParticipating, userInstance) {
  return new Promise( function(resolve, reject) {
    var overviewData = {
      name: gamesessionParticipating.name,
      phase: gamesessionParticipating.phase,
      createdAt: gamesessionParticipating.createdAt,
      // the rest are filled with extra methods below:
      teachers: [],
      userIsTeacherForSession: false,
      phase1Teams: [],
      phase2Teams: [],
      phase3Teams: []
    }
    GetTeacherUsernamesFromGameSession(gamesessionParticipating).then( function(teacherUsernames) {
      overviewData.teachers = teacherUsernames;
      CheckTeacherPriveleges(userInstance, gamesessionParticipating).then (function(userIsTeacherForSession) {
        overviewData.userIsTeacherForSession = userIsTeacherForSession;
        GetStudentTeamsMaybeFilteredByUser(!userIsTeacherForSession, userInstance, gamesessionParticipating).then( function(studentTeams) {
          MapPromise(GetPhaseNTeamData, studentTeams, 1).then( function(phase1TeamsData) {
            overviewData.phase1Teams = phase1TeamsData;
            MapPromise(GetPhaseNTeamData, studentTeams, 2).then( function(phase2TeamsData) {
              overviewData.phase2Teams = phase2TeamsData;
              GetPhase3TeamsFromStudentTeams(studentTeams).then( function(phase3teams) {
                MapPromise(GetPhase3TeamData, phase3teams).then( function(phase3TeamsData) {
                  overviewData.phase3Teams = phase3TeamsData;
                  resolve(overviewData);
                });
              });
            });
          });
        });
      });
    });
  });
}

function GetGameSessionsTeacherOnly(user_id, gamesessionsParticipating) {
  return GetGameSessionIDsTeacherOnly(user_id, gamesessionsParticipating).then( function(gamesession_ids) {
    return InstancesFromIDsPromise(gamesession_ids, GameSession).then (function (gamesessions) {
      return gamesessions;
    });
  });

}

function GetOverviewDataFromGameSessionTeacherOnly(gamesessionTeacherOnly) {
  return new Promise( function(resolve, reject) {
    var overviewData = {
      name: gamesessionTeacherOnly.name,
      phase: gamesessionTeacherOnly.phase,
      createdAt: gamesessionTeacherOnly.createdAt,
      userIsTeacherForSession: true, // (by definition)
      // the rest are filled with extra methods below:
      teachers: [],
      phase1Teams: [],
      phase2Teams: [],
      phase3Teams: []
    }
    GetTeacherUsernamesFromGameSession(gamesessionTeacherOnly).then( function(teacherUsernames) {
      overviewData.teachers = teacherUsernames;
      GetStudentTeamsForSession(gamesessionTeacherOnly).then( function(studentTeams) {
        console.log("[GetOverviewDataFromGameSessionTeacherOnly] studentTeams " + JSON.stringify(studentTeams));
        MapPromise(GetPhaseNTeamData, studentTeams, 1).then( function(phase1TeamsData) {
          overviewData.phase1Teams = phase1TeamsData;
          MapPromise(GetPhaseNTeamData, studentTeams, 2).then( function(phase2TeamsData) {
            overviewData.phase2Teams = phase2TeamsData;
            GetPhase3TeamsFromStudentTeams(studentTeams).then( function(phase3teams) {
              MapPromise(GetPhase3TeamData, phase3teams).then( function(phase3TeamsData) {
                overviewData.phase3Teams = phase3TeamsData;
                resolve(overviewData);
              });
            });
          });
        });
      });
    });
  });
}

function SessionsOverviewFromUsername(username) {
  var overviewObj = {
    username: username,
    gamesessionsTeacherOnly: [],
    gamesessionsParticipating: []
  };
  return User.findOne({
    where: { username: username }
  }).then( function(user) {
    if (user == null) {
      console.error("[SessionsOverviewFromUsername] No user found in DB for name " + username);
      return overviewObj;
    }
    else {
      var user_id = MaybeIDFromInstance(user);
      return UserXStudentTeam.findAll({
        where: {
          user_id: user_id
        }
      }).then( function(userxstudentteams) {
        return GetGameSessionsFromUserXStudentTeams(userxstudentteams).then( function(gamesessionsParticipating) {
          return MapPromise(
            GetOverviewDataFromGameSessionParticipatingAndUser,
            gamesessionsParticipating,
            user
          ).then( function(participatingOverviewData) {
            overviewObj.gamesessionsParticipating = participatingOverviewData;
            return GetGameSessionsTeacherOnly(user, gamesessionsParticipating).then( function(gamesessionsTeacherOnly) {
              return MapPromise(
                GetOverviewDataFromGameSessionTeacherOnly,
                gamesessionsTeacherOnly
              ).then( function(teacherOnlyOverviewData) {
                overviewObj.gamesessionsTeacherOnly = teacherOnlyOverviewData;
                return overviewObj;
              });
            });
          });
        });
      });
    }
  });
}
exportFunctions["SessionsOverviewFromUsername"] = SessionsOverviewFromUsername;

function CreateGameSession(name, phase) {
  return GameSession.findOrCreate({
    where: {
      name: name
    },
    defaults: {
      phase: phase
    }
  }).spread( function(gamesession, created) {
    if (!created) {
      return [gamesession, "A game session with that name already exists."];
    }
    else {
      return [gamesession, null]
    }
  });
}
exportFunctions["CreateGameSession"] = CreateGameSession;

function GetGameSession(name) {
  return GameSession.findOne({
    where: { name: name }
  });
}
exportFunctions["GetGameSession"] = GetGameSession;

function IsUser(username) {
  return User.findOne({
    where: { username: username }
  }).then( function(user) {
    return user != null;
  });
}
exportFunctions["IsUser"] = IsUser;

function GiveTeacherPriveleges(user_id, gamesession_id) {
  user_id = MaybeIDFromInstance(user_id);
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return TeacherPrivelege.findOrCreate({
    where: {
      user_id: user_id,
      gamesession_id: gamesession_id
    }
  }).spread(function (teacherprivelege, created) {
    // Doesn't matter if the TeacherPrivelege already existed.
    return teacherprivelege;
  });
}
exportFunctions["GiveTeacherPriveleges"] = GiveTeacherPriveleges;

function CheckTeacherPriveleges(user_id, gamesession_id) {
  user_id = MaybeIDFromInstance(user_id);
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return TeacherPrivelege.findOne({
    where: {
      user_id: user_id,
      gamesession_id: gamesession_id
    }
  }).then( function(teacherprivelege) {
    return teacherprivelege != null;
  });
}
exportFunctions["CheckTeacherPriveleges"] = CheckTeacherPriveleges;

function CreateStudentTeams(gamesession_id, numTeams, isPhase3) {
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return new Promise( function(resolve, reject) {
    var studentteams = [];
    for (var i = 0; i < numTeams; i++) {
      StudentTeam.create({
        gamesession_id: gamesession_id,
        is_phase3_team: isPhase3 ? true : false
      }).then( function(studentteam) {
        studentteams.push(studentteam);
        if (studentteams.length == numTeams) {
          resolve(studentteams);
        }
      })
    }
  });
}
exportFunctions["CreateStudentTeams"] = CreateStudentTeams;

function GetUsersFromUsernames(usernames) {
  return new Promise( function(resolve, reject) {
    var users = [];
    for (var i = 0; i < usernames.length; i++) {
      users.push({}); // for indexing in the next step
    };
    var counter = 0;
    for (var i = 0; i < usernames.length; i++) {
      User.findOne({
        where: { username: usernames[i] }
        //where: { username: usernames[i] },
        //attributes: { include: [[i, "query_idx"]] }
        //attributes: { include: [[sequelize.literal(i), "query_idx"]] }
        //attributes: { include: [[sequelize.cast(i, "integer"), "query_idx"]] }
        // Tried a few things to make this work, but Sequelize just never returns
        // anything, and never throws any errors as well. If it worked, it'd turn
        // this function from O(n^2) to O(n) because I'd be able to use the query
        // itself to remember the index into which its result should go. (See the
        // commented-out line below in the then function.) Instead I have to
        // re-scan the usernames array to find the correct index for each result
        // returned.
        // -NB 5/26/16
      }).then( function(user) {
        //users[user.get("query_idx")] = user;
        // waaaaa another array scan :(  (see above)
        for (var i = 0; i < usernames.length; i++) {
          if (usernames[i] == user.username) {
            users[i] = user;
          }
        }
        counter += 1;
        if (counter == usernames.length) {
          resolve(users);
        }
      });
    }
  });
}
exportFunctions["GetUsersFromUsernames"] = GetUsersFromUsernames;

function AddUsersToStudentTeam(user_ids, studentteam_id) {
  user_ids = MaybeIDsFromInstances(user_ids);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return new Promise( function(resolve, reject) {
    var userxstudentteams = [];
    for (var i = 0; i < user_ids.length; i++) {
      UserXStudentTeam.create({
        user_id: user_ids[i],
        studentteam_id: studentteam_id
      }).then( function(userxstudentteam) {
        userxstudentteams.push(userxstudentteam);
        if (userxstudentteams.length == user_ids.length) {
          resolve(userxstudentteams);
        }
      });
    }
  });
}
exportFunctions["AddUsersToStudentTeam"] = AddUsersToStudentTeam;

function DrawDesignElementCard() {
  return CardType.findOne({ where: { name: "Design Element" }}).then( function(cardtype) {
    return Card.findAndCountAll({
      where: {
        cardtype_id: cardtype.get(CardType.primaryKeyAttribute)
      },
    }).then( function(result) {
      var drawIdx = Math.floor(Math.random() * result.count);
      return result.rows[drawIdx];
    });
  });
}

function DrawUniqueDesignValueCards(numCards) {
  return CardType.findAll({ where: { name: "Design Value" }}).then( function(cardtypes) {
    var cardtype_ids = IDsFromInstances(cardtypes, CardType);
    return Card.findAndCountAll({
      where: {
        cardtype_id: cardtype_ids
      },
    }).then( function(result) {
      var cards = []
      var drawnIndices = {}
      for (var i = 0; i < numCards; i++) {
        var drawIdx = Math.floor(Math.random() * result.count);
        if (drawIdx in drawnIndices) {
          var attemptCount = 0
          do {
            drawIdx = Math.floor(Math.random() * result.count);
            attemptCount += 1;
          } while (drawnIndices[drawIdx] == true && attemptCount < 10000);
          if (attemptCount == 10000) {
            console.error("[drawDesignValueCardIDsPromise] Failed to draw design value cards; Took more than 10000 attempts to draw " + numCards + " random and unique indexes from Design Value Cards list. Are you asking for more unique indices than there are Design Value card definitions in the DB?")
            return null;
          }
        }
        cards.push(result.rows[drawIdx]);
        drawnIndices[drawIdx] = true;
      }
      return cards;
    });
  });
}

function DrawHand() {
  var hand = [];
  return DrawDesignElementCard().then( function(designElementCard) {
    hand.push(designElementCard);
    return DrawUniqueDesignValueCards(3).then( function(designValueCards) {
      for (var i = 0; i < 3; i++) {
        hand.push(designValueCards[i]);
      }
      return hand;
    });
  });
}

function DrawHands(numHands) {
  var hands = [];
  return new Promise( function(resolve, reject) {
    for (var i = 0; i < numHands; i++) {
      DrawHand().then( function(hand) {
        hands.push(hand);
        if (hands.length == numHands) {
          resolve(hands);
        }
      });
    }
  });
}

function GetSchoolDesignElementsForStudentTeam(studentteam_id) {
  return GetOrCreateSchoolDesignElementsForStudentTeam(studentteam_id, false);
}
exportFunctions["GetSchoolDesignElementsForStudentTeam"] = GetSchoolDesignElementsForStudentTeam;


function GetOrCreateSchoolDesignElementsForStudentTeam(studentteam_id, allowCreate) {
  var numSDEsPerStudentTeam = 8;
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return SchoolDesignElement.findAll({
    where: {
      studentteam_id: studentteam_id
    },
    order: [
      ["design_element_id", "DESC"],
      ["design_value_1_id", "DESC"],
      ["design_value_2_id", "DESC"],
      ["design_value_3_id", "DESC"]
    ]
  }).then( function(schooldesignelements) {
    if (schooldesignelements.length != 0) {
      console.log("[GetOrCreateSchoolDesignElementsForStudentTeam] Returning the SDEs that already exist.");
      return schooldesignelements;
    }
    else if (allowCreate) {
      console.log("[GetOrCreateSchoolDesignElementsForStudentTeam] Have to create new SDEs. allowCreate is " + JSON.stringify(allowCreate));
      return DrawHands(numSDEsPerStudentTeam).then( function(hands) {
        return new Promise( function(resolve, reject) {
          var SDEs = [];
          for (var i = 0; i < numSDEsPerStudentTeam; i++) {
            var hand = hands[i];
            SchoolDesignElement.create({
              studentteam_id: studentteam_id,
              design_element_id: hand[0].get(Card.primaryKeyAttribute),
              design_value_1_id: hand[1].get(Card.primaryKeyAttribute),
              design_value_2_id: hand[2].get(Card.primaryKeyAttribute),
              design_value_3_id: hand[3].get(Card.primaryKeyAttribute),
              title: "",
              design_features: "",
              justifications: "",
              teacher_approved: 0
            }).then( function(schooldesignelement) {
              SDEs.push(schooldesignelement);
              if (SDEs.length == numSDEsPerStudentTeam) {
                resolve(SDEs);
              }
            });
          }
        });
      });
    }
    else {
      console.error("[GetOrCreateSchoolDesignElementsForStudentTeam] allowCreate was set to " + allowCreate + ", but there are no SDEs, so returning null.");
      return null;
    }
  });
}
exportFunctions["GetOrCreateSchoolDesignElementsForStudentTeam"] = GetOrCreateSchoolDesignElementsForStudentTeam;

function CardJSONFromCardID(card_id) {
  card_id = MaybeIDFromInstance(card_id);
  return Card.findById(card_id).then( function(card) {
    if (card == null) {
      console.error("[CardJSONFromCardID] Card error: No card found with id " + card_id);
      return null;
    }
    else {
      // TODO: Lookup and attach CardResearch data
      return {
        title: card.get("title"),
        quote_text: card.get("quote_text"),
        quote_author: card.get("quote_author"),
        description: card.get("description")
      };
    }
  });
}

function GetHandJSONFromSchoolDesignElement(schooldesignelement) {
  var handJSON = {};
  return CardJSONFromCardID(schooldesignelement.get("design_element_id")).then( function(cardJSON) {
    handJSON.design_element_card = cardJSON;
    return CardJSONFromCardID(schooldesignelement.get("design_value_1_id")).then( function(cardJSON) {
      handJSON.design_value_1 = cardJSON;
      return CardJSONFromCardID(schooldesignelement.get("design_value_2_id")).then( function(cardJSON) {
        handJSON.design_value_2 = cardJSON;
        return CardJSONFromCardID(schooldesignelement.get("design_value_3_id")).then( function(cardJSON) {
          handJSON.design_value_3 = cardJSON;
          handJSON.sde_id = schooldesignelement.get("id");
          handJSON.title = schooldesignelement.get("title");
          handJSON.justifications = schooldesignelement.get("justifications");
          handJSON.design_features = schooldesignelement.get("design_features");
          handJSON.teacher_approved = schooldesignelement.get("teacher_approved");
          return handJSON;
        });
      });
    });
  });
}
exportFunctions["GetHandJSONFromSchoolDesignElement"] = GetHandJSONFromSchoolDesignElement;

function CreateChatSessions(numPhases) {
  return new Promise( function(resolve, reject) {
    var chatSessions = [];
    for (var i = 0; i < numPhases; i++) {
      ChatSession.create({
        phase: i+1
      }).then( function(chatSession) {
        chatSessions.push(chatSession);
        if (chatSessions.length == numPhases) {
          var sortedChatSessions = BlankArray(numPhases);
          for (var i = 0; i < numPhases; i++) {
            var chatSession = chatSessions[i];
            sortedChatSessions[chatSession.get("phase")-1] = chatSession;
          }
          resolve(sortedChatSessions);
        }
      });
    }
  });
}

function CreateChatSessionsForStudentTeam(numChatSessions, studentteam_id) {
  return CreateChatSessions(numChatSessions).then( function(chatsessions) {
    return {
      chatsessions: chatsessions,
      studentteam_id: studentteam_id
    }
  });
}

function AddUserToChatSession(user_id, chatsession_id) {
  user_id = MaybeIDFromInstance(user_id);
  chatsession_id = MaybeIDFromInstance(chatsession_id);
  return UserXChatSession.findOrCreate({
    where: {
      user_id: user_id,
      chatsession_id: chatsession_id,
    },
    defaults: {
      is_user_online: false,
      user_socket_session: "",
      approved_phase_state: 0,
      last_message_received_at: new Date()
    }
  }).spread( function(userxchatsession, created) {
    return userxchatsession;
  });
}

function AddTeachersToChatSessions(chatsession_ids, gamesession_id) {
  chatsession_ids = MaybeIDsFromInstances(chatsession_ids);
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return TeacherPrivelege.findAll({
    where: {
      gamesession_id: gamesession_id
    }
  }).then( function(teacherPrivileges) {
    var user_ids = GetColumnsFromInstances(teacherPrivileges, "user_id");
    console.log("[AddTeachersToChatSessions] Starting CrossMapPromise for AddUserToChatSession with user_ids " + JSON.stringify(user_ids) + " and chatsession_ids " + JSON.stringify(chatsession_ids));
    return CrossMapPromise(
      AddUserToChatSession,
      user_ids,
      chatsession_ids
    );
  });
}

function PreparePhaseNInstance(studentteam_id, phase) {
  return new Promise( function(resolve, reject) {
    ChatSession.create({ phase: phase }).then( function(chatsession) {
      PhaseInstance.create({
        chatsession_id: chatsession["id"],
        studentteam_id: studentteam_id,
        phase: phase,
        teacher_approved: 0
      }).then( function(phaseinstance) {
        var usersInTeamFunc = null;
        switch(phase) {
          case 3:                   usersInTeamFunc = GetUsersInPhase3Team; break;
          case 1: case 2: default:  usersInTeamFunc = GetUsersInTeam; break;
        }
        usersInTeamFunc(studentteam_id).then( function(users) {
          MapPromise(
            AddUserToChatSession,
            users,
            chatsession["id"]
          ).then( function(userxchatsessions) {
            resolve({
              phaseinstance: phaseinstance,
              chatsession: chatsession,
              userxchatsessions: userxchatsessions
            });
          });
        });
      });
    });
  });
}

function PreparePhaseNInstances(studentteam_ids, phase) {
  var studentteam_ids = MaybeIDsFromInstances(studentteam_ids);
  var numPhaseNInstances = 0;
  var numChatSessions = 0;
  var numUserXChatSessions = 0;
  var numTeacherUserXChatSessions = 0;
  return new Promise( function(resolve, reject) {
    MapPromise(
      PreparePhaseNInstance,
      studentteam_ids,
      phase
    ).then( function(phaseNPrepObjs) {
      console.log("[PreparePhaseNInstances] Got phase preparation objects: " + JSON.stringify(phaseNPrepObjs));
      numPhaseNInstances += GetColumnsFromInstances(phaseNPrepObjs, "phaseinstance").length;
      var chatsessions = GetColumnsFromInstances(phaseNPrepObjs, "chatsession");
      numChatSessions += chatsessions.length;
      var userxchatsessionArrays = GetColumnsFromInstances(phaseNPrepObjs, "userxchatsessions");
      for (var i = 0; i < userxchatsessionArrays.length; i++) {
        numUserXChatSessions += userxchatsessionArrays[i].length;
      }
      console.log("[PreparePhaseNInstances] Adding teachers. Getting gamesession first, using student id " + studentteam_ids[0]);
      StudentTeam.findById(studentteam_ids[0]).then( function(studentTeam) {
        console.log("[PreparePhaseNInstances] Adding teachers. Got gamesession " + studentTeam["gamesession_id"]);
        AddTeachersToChatSessions(chatsessions, studentTeam["gamesession_id"]).then( function(teacher_userxchatsessions) {
          console.log("[PreparePhaseNInstances] Got userxchatsessions for teachers: " + JSON.stringify(teacher_userxchatsessions));
          numTeacherUserXChatSessions += teacher_userxchatsessions.length;
          resolve({
            numPhaseNInstances: numPhaseNInstances,
            numChatSessions: numChatSessions,
            numUserXChatSessions: numUserXChatSessions,
            numTeacherUserXChatSessions: numTeacherUserXChatSessions
          });
        });
      });
    });
  });
}
exportFunctions["PreparePhaseNInstances"] = PreparePhaseNInstances;

function PreparePhase1And2Instances(studentteam_ids) {
  return new Promise( function(resolve, reject) {
    PreparePhaseNInstances(studentteam_ids, 1).then( function(counts1) {
      PreparePhaseNInstances(studentteam_ids, 2).then( function(counts2) {
        var responseObj = {
          numPhaseNInstances: counts1.numPhaseNInstances + counts2.numPhaseNInstances,
          numChatSessions: counts1.numChatSessions + counts2.numChatSessions,
          numUserXChatSessions: counts1.numUserXChatSessions + counts2.numUserXChatSessions,
          numTeacherUserXChatSessions: counts1.numTeacherUserXChatSessions + counts2.numTeacherUserXChatSessions
        };
        console.log("[PreparePhase1And2Instances] Returning " + JSON.stringify(responseObj));
        resolve(responseObj);
      });
    });
  });
}
exportFunctions["PreparePhase1And2Instances"] = PreparePhase1And2Instances;

function PreparePhase3Instances(phase3team_ids) {
  return new Promise( function(resolve, reject) {
    PreparePhaseNInstances(phase3team_ids, 3).then( function(counts) {
      console.log("[PreparePhase3Instances] Returning " + JSON.stringify(counts));
      resolve(counts);
    });
  });
}
exportFunctions["PreparePhase3Instances"] = PreparePhase3Instances;

function GetPhaseInstanceForTeamAndPhase(studentteam_id, phase) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return PhaseInstance.findOne({
    where: {
      studentteam_id: studentteam_id,
      phase: phase
    }
  });
}
exportFunctions["GetPhaseInstanceForTeamAndPhase"] = GetPhaseInstanceForTeamAndPhase;

function GetChatHistoryForPhaseInstance(phaseinstance) {
  return MaybeInstanceFromIDPromise(phaseinstance, PhaseInstance).then( function(phaseinstance) {
    return ChatMessage.findAll({
      where: {
        chatsession_id: phaseinstance.get("chatsession_id")
      },
      order: [
        ["createdAt", "DESC"]
      ]
    });
  });
}
exportFunctions["GetChatHistoryForPhaseInstance"] = GetChatHistoryForPhaseInstance;

function PushChatMessageForPhaseInstance(phaseinstance, username, user_color, message) {
  return MaybeInstanceFromIDPromise(phaseinstance, PhaseInstance).then( function(phaseinstance) {
    console.log("CREATING");
    console.log("Going to create with: " + JSON.stringify({
      chatsession_id: phaseinstance.get("chatsession_id"),
      username: username,
      user_color: user_color,
      message: message
    }));
    return ChatMessage.create({
      chatsession_id: phaseinstance.get("chatsession_id"),
      username: username,
      user_color: user_color,
      message: message
    }).then( function(chatmessage) {
      console.log("Got back chatmessage: " + JSON.stringify(chatmessage));
      return chatmessage;
    });
  });
}
exportFunctions["PushChatMessageForPhaseInstance"] = PushChatMessageForPhaseInstance;

function CreatePhase2DataFromPhase1Data(gamesession_id) {
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return GetStudentTeamsForSession(gamesession_id).then( function(studentTeams) {
    return SchoolDesignElement.findAndCountAll({
      where: {
        studentteam_id: IDsFromInstances(studentTeams, StudentTeam),
        design_features: {
          $not: "" // don't include SDEs with empty design_features
        }
      }
    }).then( function(schooldesignelementsResults) {
      if (schooldesignelementsResults.count == 0) {
        return [];
      }
      else {
        var schooldesignelements = schooldesignelementsResults.rows;
        console.log("[CreatePhase2DataFromPhase1Data] Got schooldesignelements to push to Phase2Choices: " + JSON.stringify(schooldesignelements));
        var phase2choiceObjs = [];
        var studentteam_ids = IDsFromInstances(studentTeams, StudentTeam);
        var sde_ids = IDsFromInstances(schooldesignelements, SchoolDesignElement);
        for (var i = 0; i < studentteam_ids.length; i++) {
          var studentteam_id = studentteam_ids[i];
          for (var j = 0; j < schooldesignelements.length; j++) {
            var sde_id = sde_ids[j];
            if (schooldesignelements[j].studentteam_id == studentteam_id) {
              phase2choiceObjs.push({
                studentteam_id: studentteam_id,
                schooldesignelement_id: sde_id,
                chosen_for_school: false,
                justifications: ""
              });
            }
          }
        }
        console.log("[CreatePhase2DataFromPhase1Data] Creating Phase2Choices with the following data: " + JSON.stringify(phase2choiceObjs));
        return Phase2Choice.bulkCreate(phase2choiceObjs).then( function() {
          //bulkCreate requires re-query
          return Phase2Choice.findAll({
            where: {
              $and: {
                studentteam_id: studentteam_ids,
                schooldesignelement_id: sde_ids
              }
            }
          }).then( function(phase2choices) {
            return phase2choices;
          });
        })
        // construct construction objects, do bulkCreate, return find by SDEs, StudentTeams
      }
    });
  });
}
exportFunctions["CreatePhase2DataFromPhase1Data"] = CreatePhase2DataFromPhase1Data;

function SetGameSessionPhase(gamesession_id, phase) {
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return GameSession.findById(gamesession_id).then( function(gamesession) {
    gamesession.phase = phase;
    return gamesession.save();
  });
}
exportFunctions["SetGameSessionPhase"] = SetGameSessionPhase;

function SaveSchoolDesignElement(sde_id, title, features) {
  return SchoolDesignElement.findById(sde_id).then( function(schooldesignelement) {
    if (schooldesignelement == null) {
      return null;
    }
    else {
      schooldesignelement.title = title;
      schooldesignelement.design_features = features;
      return schooldesignelement.save();
    }
  });
}
exportFunctions["SaveSchoolDesignElement"] = SaveSchoolDesignElement;

function GetOrCreateSDEApprovalForUserandSDE(user_id, schooldesignelement_id) {
  user_id = MaybeIDFromInstance(user_id);
  schooldesignelement_id = MaybeIDFromInstance(schooldesignelement_id);
  return SDEApproval.find({
    where: {
      user_id: user_id,
      schooldesignelement_id: schooldesignelement_id
    }
  }).then( function(sdeapproval) {
    if (sdeapproval == null) {
      return SDEApproval.create({
        user_id: user_id,
        schooldesignelement_id: schooldesignelement_id,
        approved: 0
      });
    }
    else {
      return sdeapproval;
    }
  });
}

function GetUserXChatSessionForUserAndTeamAndPhase(user_id, studentteam_id, phase) {
  user_id = MaybeIDFromInstance(user_id);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return GetPhaseInstanceForTeamAndPhase(studentteam_id, phase).then( function(phaseinstance) {
    console.log("[GetUserXChatSessionForUserAndTeamAndPhase] Got phaseinstance: " + JSON.stringify(phaseinstance));
    if (phaseinstance == null) {
      console.error("[GetUserXChatSessionForUserAndTeamAndPhase] Got null phaseinstance for team id " + JSON.stringify(studentteam_id) + " and phase " + phase);
    }
    return UserXChatSession.findOne({
      where: {
        user_id: user_id,
        chatsession_id: phaseinstance["chatsession_id"]
      }
    }).then( function(userxchatsession) {
      if (userxchatsession == null) {
        console.error("[GetUserXChatSessionForUserAndTeamAndPhase] No UserXChatSession record found for user_id " + JSON.stringify(user_id) + ", studentteam_id " + JSON.stringify(studentteam_id) + ", and phase " + phase + "; got phaseinstance " + JSON.stringify(phaseinstance) + " and looked for chatsession_id: " + phaseinstance["chatsession_id"]);
        return null;
      }
      else {
        return userxchatsession;
      }
    });
  });
}
exportFunctions["GetUserXChatSessionForUserAndTeamAndPhase"] = GetUserXChatSessionForUserAndTeamAndPhase;

function SetUserOnlineForUserAndStudentTeamAndPhase(user_id, studentteam_id, phase, isOnline) {
  return GetUserXChatSessionForUserAndTeamAndPhase(user_id, studentteam_id, phase).then( function(userxchatsession) {
    userxchatsession.is_user_online = isOnline;
    return userxchatsession.save();
  });
}
exportFunctions["SetUserOnlineForUserAndStudentTeamAndPhase"] = SetUserOnlineForUserAndStudentTeamAndPhase;

function GetUserOnlineForUserAndStudentTeamPhasePair(user_id, studentTeamIDAndPhase) {
  user_id = MaybeIDFromInstance(user_id);
  var studentteam_id = studentTeamIDAndPhase[0];          // SUPER JANK. TODO FIXME -NB
  var phase = studentTeamIDAndPhase[1];                   // But MapPromise only allows one extraArg.
  studentteam_id = MaybeIDFromInstance(studentteam_id);   // Solution is to make MapPromise better.
  console.log("[GetUserOnlineForUserAndStudentTeamPhasePair] Calling GetPhaseInstanceForTeamAndPhase with: studentteam id " + studentteam_id + " and phase " + phase);
  return GetUserXChatSessionForUserAndTeamAndPhase(user_id, studentteam_id, phase).then( function(userxchatsession) {
    console.log("[GetUserOnlineForUserAndStudentTeamPhasePair] Returning is_user_online from " + JSON.stringify(userxchatsession));
    return userxchatsession["is_user_online"];
  });

}

function GetUserOnlineForUserAndStudentTeamAndPhase(user_id, studentteam_id, phase) {
  return GetUserOnlineForUserAndStudentTeamPhasePair(user_id, [studentteam_id, phase]);
}
exportFunctions["GetUserOnlineForUserAndStudentTeamAndPhase"] = GetUserOnlineForUserAndStudentTeamAndPhase;

function GetPhase1SessionApprovalStatusForStudentTeam(studentteam_id) {
  return new Promise( function(resolve, reject) {
    studentteam_id = MaybeIDFromInstance(studentteam_id);
    StudentTeam.findById(studentteam_id).then( function(studentteam) {
      if (studentteam == null) {
        console.error("[GetPhase1SessionApprovalStatus] Got back a null studentteam for user_id: " + JSON.stringify(user_id) + " and gamesession_id: " + JSON.stringify(gamesession_id));
      }
      // get all SDEs for the student team
      GetOrCreateSchoolDesignElementsForStudentTeam(studentteam, false).then( function(schooldesignelements) {
        if (schooldesignelements == null) {
          console.error("[GetPhase1SessionApprovalStatusForStudentTeam] Got no SDEs for studentteam: " + JSON.stringify(studentteam) + "; returning null.");
          resolve(null);
        }
        else {
          GetUsersInTeam(studentteam).then( function(users) {
            if (users == null) {
              console.error("[GetPhase1SessionApprovalStatusForStudentTeam] Got null users response for studentteam: " + JSON.stringify(studentteam));
            }
            console.log("[GetPhase1SessionApprovalStatusForStudentTeam] Got users for studentteam: " + JSON.stringify(users));
            var sdeApprovalStatuses = [];
            for (var i = 0; i < schooldesignelements.length; i++) {
              MapPromiseWithReturnArg(
                GetOrCreateSDEApprovalForUserandSDE,
                users,
                schooldesignelements[i]["id"],
                i // returnArg: schooldesignelements array index
              ).then( function(sdeApprovalsAndIndex) {
                console.log("[GetPhase1SessionApprovalStatusForStudentTeam] Got user approvals response for a schooldesignelement: " + JSON.stringify(sdeApprovalsAndIndex));
                var sdeApprovals = sdeApprovalsAndIndex["result"];
                var sdeIndex = sdeApprovalsAndIndex["returnArg"];
                var userApprovalsForSDE = [];
                MapPromise(
                  GetUserOnlineForUserAndStudentTeamPhasePair,
                  users,
                  [studentteam_id, 1]
                ).then( function(userOnlineBools) {
                  for (var i = 0; i < sdeApprovals.length; i++) {
                    userApprovalsForSDE.push({
                      username: users[i]["username"], // MapPromise preserves order
                      is_user_online: userOnlineBools[i],
                      approved: sdeApprovals[i]["approved"]
                    });
                  }
                  sdeApprovalStatuses.push({
                    sde_id: schooldesignelements[sdeIndex]["id"],
                    userApprovals: userApprovalsForSDE
                  });
                  if (sdeApprovalStatuses.length == schooldesignelements.length) {
                    resolve(sdeApprovalStatuses);
                  }
                });
              });
            }
          });
        }
      });
    });
  });
  /*
  {
    usernameAndApprovalObjects: [
      {
        sde_id: %SDE_ID_1%,
        userApprovals: [
          {
            username: %USERNAME%,
            approved: %INTEGER%
          },
          ...
        ]
      },
      ...
    ]
  }
*/
}
exportFunctions["GetPhase1SessionApprovalStatusForStudentTeam"] = GetPhase1SessionApprovalStatusForStudentTeam;

function SetSDEApprovalForUserAndSDE(user_id, sde_id, approved) {
  user_id = MaybeIDFromInstance(user_id);
  sde_id = MaybeIDFromInstance(sde_id);
  return SDEApproval.findOrCreate({
    where: {
      user_id: user_id,
      schooldesignelement_id: sde_id
    },
    defaults: {
      approved: approved
    }
  }).spread( function(sdeApproval, created) {
    if (!created) {
      sdeApproval.approved = approved;
      return sdeApproval.save();
    }
    else {
      return sdeApproval;
    }
  });
}
exportFunctions["SetSDEApprovalForUserAndSDE"] = SetSDEApprovalForUserAndSDE;

function GetStudentTeamFromID(sde_id) {
  sde_id = MaybeIDFromInstance(sde_id);
  return StudentTeam.findById(sde_id);
}
exportFunctions["GetStudentTeamFromID"] = GetStudentTeamFromID;

function CheckUserInTeam(user_id, studentteam_id) {
  user_id = MaybeIDFromInstance(user_id);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return UserXStudentTeam.findOne({
    where: {
      user_id: user_id,
      studentteam_id: studentteam_id
    }
  });
}
exportFunctions["CheckUserInTeam"] = CheckUserInTeam;

function GetPhase2ChoicesForStudentTeam(studentteam_id) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return Phase2Choice.findAll({
    where: {
      studentteam_id: studentteam_id
    }
  });
}
exportFunctions["GetPhase2ChoicesForStudentTeam"] = GetPhase2ChoicesForStudentTeam;

function GetPhase2HandDataForStudentTeam(studentteam_id) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return GetPhase2ChoicesForStudentTeam(studentteam_id).then( function(phase2Choices) {
    if (phase2Choices == null || phase2Choices.length == 0) {
      console.error("[GetPhase2HandDataForStudentTeam] For studentteam id " + JSON.stringify(studentteam_id) + ", returned no phase2choices: " + JSON.stringify(phase2Choices));
      return null;
    }
    console.log("[GetPhase2HandDataForStudentTeam] For studentteam id " + JSON.stringify(studentteam_id) + ", got phase2choices: " + JSON.stringify(phase2Choices));
    var sde_ids = GetColumnsFromInstances(phase2Choices, "schooldesignelement_id");
    return InstancesFromIDsPromise(sde_ids, SchoolDesignElement).then( function(schooldesignelements) {
      return MapPromise(
        GetHandJSONFromSchoolDesignElement,
        schooldesignelements
      ).then( function(handJSONs) {
        console.log("[GetPhase2HandDataForStudentTeam] Got back handJSONs: " + JSON.stringify(handJSONs));
        for (var i = 0; i < phase2Choices.length; i++) {
          // handJSONs/phase2Choices should be 1-to-1
          handJSONs[i].chosen_for_school = phase2Choices[i].chosen_for_school;
        }
        return handJSONs;
      });
    });
  });
}
exportFunctions["GetPhase2HandDataForStudentTeam"] = GetPhase2HandDataForStudentTeam;

function SetPhase2ChoiceForStudentTeamAndSDE(studentteam_id, sde_id, chosen_for_school) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  sde_id = MaybeIDFromInstance(sde_id);
  return Phase2Choice.findOne({
    where: {
      studentteam_id: studentteam_id,
      schooldesignelement_id: sde_id
    }
  }).then( function(phase2choice) {
    if (phase2choice == null) {
      return null;
    }
    else {
      phase2choice.chosen_for_school = chosen_for_school;
      return phase2choice.save();
    }
  });
}
exportFunctions["SetPhase2ChoiceForStudentTeamAndSDE"] = SetPhase2ChoiceForStudentTeamAndSDE;

function PrepareSchoolDesignElementsForStudentTeams(studentteam_ids) {
  studentteam_ids = MaybeIDsFromInstances(studentteam_ids);
  return MapPromise(
    GetOrCreateSchoolDesignElementsForStudentTeam,
    studentteam_ids,
    true
  ).then( function(schooldesignelement_lists) {
    if (schooldesignelement_lists.length == 0) {
      console.error("[PrepareSchoolDesignElementsForStudentTeams] Error! Failed to create any SchoolDesignElements for studentteam ids: " + studentteam_ids);
    }
    else {
      return schooldesignelement_lists;
    }
  });
}
exportFunctions["PrepareSchoolDesignElementsForStudentTeams"] = PrepareSchoolDesignElementsForStudentTeams;

function GetPhase2ApprovalForUserAndStudentTeam(user_id, studentteam_id) {
  user_id = MaybeIDFromInstance(user_id);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return PhaseInstance.findOne({
    where: {
      studentteam_id: studentteam_id,
      phase: 2
    }
  }).then( function(phaseinstance) {
    if (phaseinstance == null) {
      console.error("[GetPhase2ApprovalForUserAndStudentTeam] No phase instance found for phase 2 with studentteam_id: " + studentteam_id);
      return null;
    }
    else {
      return UserXChatSession.findOne({
        where: {
          user_id: user_id,
          chatsession_id: phaseinstance["chatsession_id"]
        }
      }).then( function(userxchatsession) {
        if (userxchatsession == null) {
          console.error("[GetPhase2ApprovalForUserAndStudentTeam] No UserXChatSession record found for user_id " + user_id + " and chatsession_id " + phaseinstance["chatsession_id"]);
          return null;
        }
        else {
          console.log("[GetPhase2ApprovalForUserAndStudentTeam] Got userxchatsession: " + JSON.stringify(userxchatsession) + ", returning " + userxchatsession["approved_phase_state"]);
          return userxchatsession["approved_phase_state"];
        }
      });
    }
  });
}

function GetPhase2Approvals(studentteam_id) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return GetUsersInTeam(studentteam_id).then( function(users) {
    return MapPromise(
      GetPhase2ApprovalForUserAndStudentTeam,
      users,
      studentteam_id
    ).then( function(userApprovedPhaseStates) {
      return MapPromise(
        GetUserOnlineForUserAndStudentTeamPhasePair,
        users,
        [studentteam_id, 2]
      ).then( function(userOnlineBools) {
        var approvalStates = [];
        for (var i = 0; i < userApprovedPhaseStates.length; i++) {
          approvalStates.push({
            username: users[i]["username"], // MapPromise preserves ordering
            is_user_online: userOnlineBools[i],
            approved: userApprovedPhaseStates[i] // integer, -1 / 1
          });
        }
        return approvalStates;
      });
    });
  });
  /*
  [
    {
    username: string,
    approved: integer (-1 / 1)
    },
    ...
  ]
  */
}
exportFunctions["GetPhase2Approvals"] = GetPhase2Approvals;

function SetTeacherApprovedForPhaseInstance(phaseinstance, teacher_approved) {
  phaseinstance.teacher_approved = teacher_approved;
  return phaseinstance.save();
}
exportFunctions["SetTeacherApprovedForPhaseInstance"] = SetTeacherApprovedForPhaseInstance;

function SetPhase2ApprovalForUserAndStudentTeam(user_id, studentteam_id, approved_phase_state) {
  user_id = MaybeIDFromInstance(user_id);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return GetPhaseInstanceForTeamAndPhase(studentteam_id, 2).then( function(phaseinstance) {
    return UserXChatSession.findOne({
      where: {
        user_id: user_id,
        chatsession_id: phaseinstance["chatsession_id"]
      }
    }).then( function(userxchatsession) {
      userxchatsession.approved_phase_state = approved_phase_state;
      return userxchatsession.save();
    });
  });
}
exportFunctions["SetPhase2ApprovalForUserAndStudentTeam"] = SetPhase2ApprovalForUserAndStudentTeam;

function GetTeamSummaryForSession(gamesession_id) {
  return GetStudentTeamsForSession(gamesession_id).then( function(studentteams) {
    return MapPromise(
      function(studentteam_id) {
        return GetUsersInTeam(studentteam_id).then( function(users) {
          return GetColumnsFromInstances(users, "username");
        });
      },
      studentteams
    );
  });
}
exportFunctions["GetTeamSummaryForSession"] = GetTeamSummaryForSession;

function GetUniqueStudentTeamsForUsersAndSession(users, gamesession_id) {
  var studentTeamsEntered = {};
  var uniqueStudentTeams = [];
  return MapPromise(
    GetStudentTeamForUserInGameSession,
    users,
    gamesession_id
  ).then( function(studentteams) {
    console.log("[GetUniqueStudentTeamsForUsersAndSession] After MapPromise, got studentteams: " + JSON.stringify(studentteams));
    for (var i = 0; i < studentteams.length; i++) {
      if (studentTeamsEntered[studentteams[i]["id"]] === undefined) {
        uniqueStudentTeams.push(studentteams[i]);
        studentTeamsEntered[studentteams[i]["id"]] = true;
        console.log("[GetUniqueStudentTeamsForUsersAndSession] Pushed studentteam_id: " + studentteams[i]["id"]);
      }
      else {
        console.log("[GetUniqueStudentTeamsForUsersAndSession] Not pushing studentteam_id: " + studentteams[i]["id"]);
      }
    }
    console.log("[GetUniqueStudentTeamsForUsersAndSession] Returning: " + JSON.stringify(uniqueStudentTeams));
    return uniqueStudentTeams;
  });
}

function GetStudentTeamArraysFromUserArraysAndSession(userArrays, gamesession_id) {
  console.log("[GetStudentTeamArraysFromUserArraysAndSession] Mapping GetUniqueStudentTeamsForUsersAndSession with userArrays " + JSON.stringify(userArrays));
  gamesession_id = MaybeIDFromInstance(gamesession_id);
  return MapPromise(
    GetUniqueStudentTeamsForUsersAndSession,
    userArrays,
    gamesession_id
  );
}
exportFunctions["GetStudentTeamArraysFromUserArraysAndSession"] = GetStudentTeamArraysFromUserArraysAndSession;

function CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams(phase3team, studentteams) {
  console.log("[CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams] got phase3team: " + JSON.stringify(phase3team));
  console.log("[CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams] got studentteams: " + JSON.stringify(studentteams));
  var phase3team_id = MaybeIDFromInstance(phase3team);
  var studentteam_ids = MaybeIDsFromInstances(studentteams);
  return MapPromise(
    function(studentteam_id, phase3team_id) {
      console.log("[CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams] Creating Phase3MergedMembership with : " + JSON.stringify({
        phase3team_id: phase3team_id,
        studentteam_id: studentteam_id
      }));
      return Phase3MergedMembership.findOrCreate({
        where: {
          phase3team_id: phase3team_id,
          studentteam_id: studentteam_id
        }
      }).spread( function(phase3mergedmembership, created) {
        console.log("[CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams] phase3mergedmembership created.");
        return phase3mergedmembership;
      });
    },
    studentteam_ids,
    phase3team_id
  );
}
exportFunctions["CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams"] = CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams;

function GetDesignElementCards() {
  return CardType.findOne({ where: { name: "Design Element" }}).then( function(cardtype) {
    return Card.findAll({
      where: {
        cardtype_id: cardtype.get(CardType.primaryKeyAttribute)
      },
    }).then( function(cards) {
      console.log("[GetDesignElementCards] Returning cards: " + JSON.stringify(cards));
      return cards;
    });
  });
}

function PreparePhase3SlotsForPhase3Team(phase3team_id) {
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return GetDesignElementCards().then( function(designElementCards) {
    return MapPromise(
      function(designelementcard_id, phase3team_id) {
        designelementcard_id = MaybeIDFromInstance(designelementcard_id);
        return Phase3Slot.findOrCreate({
          where: {
            phase3team_id: phase3team_id,
            graycard_id: designelementcard_id
          }
        }).spread( function(phase3slot, created) {
          console.log("[PreparePhase3SlotsForPhase3Team] Got/Created phase3slot with: " + JSON.stringify({
            phase3team_id: phase3team_id,
            graycard_id: designelementcard_id
          }));
          return phase3slot;
        });
      },
      designElementCards,
      phase3team_id
    );
  });
}

function PreparePhase3SlotsForPhase3Teams(phase3teams) {
  console.log("[PreparePhase3SlotsForPhase3Teams] Mapping using phase3teams: " + JSON.stringify(phase3teams));
  return MapPromise(
    PreparePhase3SlotsForPhase3Team,
    phase3teams
  );
}
exportFunctions["PreparePhase3SlotsForPhase3Teams"] = PreparePhase3SlotsForPhase3Teams;

function SetTeacherApprovedForSDE(sde_id, approved) {
  sde_id = MaybeIDFromInstance(sde_id);
  return SchoolDesignElement.findById(sde_id).then( function(schooldesignelement) {
    schooldesignelement.teacher_approved = approved;
    return schooldesignelement.save();
  });
}
exportFunctions["SetTeacherApprovedForSDE"] = SetTeacherApprovedForSDE;

function GetPhase3TeamIDForStudentTeam(studentteam_id) {
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return Phase3MergedMembership.findOne({
    where: {
      studentteam_id: studentteam_id
    }
  }).then( function(phase3mergedmembership) {
    return phase3mergedmembership["phase3team_id"];
  });
}
exportFunctions["GetPhase3TeamIDForStudentTeam"] = GetPhase3TeamIDForStudentTeam;

function CheckUserInPhase3Team(user_id, phase3team_id) {
  user_id = MaybeIDFromInstance(user_id);
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return Phase3MergedMembership.findAll({
    where: {
      phase3team_id: phase3team_id
    }
  }).then( function(phase3mergedmemberships) {
    if (phase3mergedmemberships.length == 0) {
      return false;
    }
    else {
      return MapPromise(
        function(studentteam_id, user_id) {
          return CheckUserInTeam(user_id, studentteam_id)
        },
        GetColumnsFromInstances(phase3mergedmemberships, "studentteam_id"),
        user_id
      ).then( function(booleans) {
        console.log("[CheckUserInPhase3Team] got booleans: " + JSON.stringify(booleans));
        for (var i = 0; i < booleans.length; i++) {
          if (booleans[i]) {
            return true;
          }
        }
        return false;
      });
    }
  });
}
exportFunctions["CheckUserInPhase3Team"] = CheckUserInPhase3Team;

function GetStudentTeamsForPhase3Team(phase3team_id) {
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return Phase3MergedMembership.findAll({
    where: {
      phase3team_id: phase3team_id
    }
  }).then( function(phase3mergedmemberships) {
    console.log("[GetStudentTeamsForPhase3Team] Got phase3mergedmemberships: " + phase3mergedmemberships);
    return InstancesFromIDsPromise(GetColumnsFromInstances(phase3mergedmemberships, "studentteam_id"), StudentTeam);
  });
}

function GetPossibleSDEsForPhase3SlotAndTeam(phase3slot, phase3team_id) {
  return new Promise( function(resolve, reject) {
    phase3team_id = MaybeIDFromInstance(phase3team_id);
    GetStudentTeamsForPhase3Team(phase3team_id).then( function(studentTeams) {
      Phase2Choice.findAll({
        where: {
          studentteam_id: GetColumnsFromInstances(studentTeams, "id"),
          chosen_for_school: true
        }
      }).then( function(phase2choices) {
        if (phase2choices == null) {
          console.error("[GetPossibleSDEIDsForPhase3Team] Got no valid phase2choices for student teams: " + JSON.stringify(studentTeams) + "; (chosen_for_school must be true!) Resolving null.");
          resolve(null);
        }
        else {
          var allSDEids = GetColumnsFromInstances(phase2choices, "schooldesignelement_id");
          // filter by graycard_id
          InstancesFromIDsPromise(allSDEids, SchoolDesignElement).then( function(allSDEs) {
            var filteredSDEs = [];
            for (var i = 0; i < allSDEs.length; i++) {
              if (allSDEs[i]["design_element_id"] == phase3slot["graycard_id"]) {
                filteredSDEs.push(allSDEs[i]);
              }
            }
            console.log("[GetPossibleSDEsForPhase3SlotAndTeam] Got filteredSDEs: " + filteredSDEs);
            resolve(filteredSDEs);
          });
        }
      });
    });
  });
}

function GetDesignValueTitlesForSDE(SDE) {
  return Card.findById(SDE["design_value_1_id"]).then( function(design_value_1) {
    return Card.findById(SDE["design_value_2_id"]).then( function(design_value_2) {
      return Card.findById(SDE["design_value_3_id"]).then( function(design_value_3) {
        return {
          design_value_1_title: design_value_1["title"],
          design_value_2_title: design_value_2["title"],
          design_value_3_title: design_value_3["title"]
        };
      });
    });
  });
}

function GetPossibleSDEDataForPhase3SlotAndTeam(phase3slot, phase3team_id) {
  return new Promise( function(resolve, reject) {
    GetPossibleSDEsForPhase3SlotAndTeam(phase3slot, phase3team_id).then( function(SDEs) {
      console.log("[GetPossibleSDEDataForPhase3SlotAndTeam] Got SDEs: " + JSON.stringify(SDEs));
      MapPromise(
        GetDesignValueTitlesForSDE,
        SDEs
      ).then( function(designValueTitlesObjs) {
        var SDEDatas = [];
        for (var i = 0; i < SDEs.length; i++) {
          var SDE = SDEs[i];
          SDEDatas.push({
            sde_id: SDE["id"],
            title: SDE["title"],
            design_features: SDE["design_features"],
            justifications: SDE["justifications"],
            teacher_approved: SDE["teacher_approved"], // here, this should always be true
            design_value_1_title: designValueTitlesObjs[i]["design_value_1_title"],
            design_value_2_title: designValueTitlesObjs[i]["design_value_2_title"],
            design_value_3_title: designValueTitlesObjs[i]["design_value_3_title"]
          });
        }
        resolve(SDEDatas);
      });
    });
  });
}

function GetOrCreatePhase3ChoiceForSlot(phase3slot_id) {
  phase3slot_id = MaybeIDFromInstance(phase3slot_id);
  return Phase3Choice.findOrCreate({
    where: {
      phase3slot_id: phase3slot_id
    },
    defaults: {
      schooldesignelement_id_unsafe: 0,
      justifications: ""
    }
  }).spread( function(phase3Choice, created) {
    return phase3Choice;
  });
}

function GetPhase3SlotDataForTeam(phase3slot, phase3team_id) {
  return new Promise( function(resolve, reject) {
    CardJSONFromCardID(phase3slot["graycard_id"]).then( function(graycardJSON) {
      console.log("[GetPhase3SlotDataForTeam] Got graycardJSON: " + JSON.stringify(graycardJSON));
      GetPossibleSDEDataForPhase3SlotAndTeam(phase3slot, phase3team_id).then( function(possibleSDEData) {
        GetOrCreatePhase3ChoiceForSlot(phase3slot).then( function(phase3choice) {
          console.log("[GetPhase3SlotDataForTeam] Got phase3choice: " + JSON.stringify(phase3choice));
          var responseObj = {
            slot_id: phase3slot["id"],
            graycard_data: graycardJSON,
            possible_SDEs: possibleSDEData,
            chosen_sde_id: phase3choice["schooldesignelement_id_unsafe"],
          }
          resolve(responseObj);
        });
      });
    });
  });
}

function GetPhase3SDEDataForTeam(phase3team_id) {
  return new Promise( function(resolve, reject) {
    phase3team_id = MaybeIDFromInstance(phase3team_id);
    Phase3Slot.findAll({
      where: {
        phase3team_id: phase3team_id
      }
    }).then( function(phase3slots) {
      console.log("[GetPhase3SDEDataForTeam] Got phase3slots: " + JSON.stringify(phase3slots));
      MapPromise(
        GetPhase3SlotDataForTeam,
        phase3slots,
        phase3team_id
      ).then( function(phase3SlotDatas) {
        console.log("[GetPhase3SDEDataForTeam] Got phase3SlotDatas: " + JSON.stringify(phase3SlotDatas));
        var responseObj = {
          graycard_slots: []
        };
        for (var i = 0; i < phase3SlotDatas.length; i++) {
          responseObj.graycard_slots.push(phase3SlotDatas[i]);
        }
        console.log("[GetPhase3SDEDataForTeam] returning " + JSON.stringify(responseObj));
        resolve(responseObj);
      });
    });
  });
  /*
    {
      graycard_slots: [
        {
          slot_id: int,
          chosen_sde_id: int,
          graycard_data: {
            title: string,
            description: string,
            quote_text: string,
            quote_author: string
          },
          possible_SDEs: [
            {
              sde_id: int,
              title: string,
              design_features: string,
              justifications: string,
              teacher_approved: boolean, // all of these should be true
              design_value_1_title: string,
              design_value_2_title: string,
              design_value_3_title: string
            },
            ...
          ]
        },
        ...
      ]
    }
  */
}
exportFunctions["GetPhase3SDEDataForTeam"] = GetPhase3SDEDataForTeam;

function GetPhase3ApprovalForUserAndStudentTeam(user_id, phase3team_id) {
  user_id = MaybeIDFromInstance(user_id);
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return PhaseInstance.findOne({
    where: {
      studentteam_id: phase3team_id,
      phase: 3
    }
  }).then( function(phaseinstance) {
    if (phaseinstance == null) {
      console.error("[GetPhase3ApprovalForUserAndStudentTeam] No phase instance found for phase 3 with phase3team_id: " + phase3team_id);
      return null;
    }
    else {
      return UserXChatSession.findOne({
        where: {
          user_id: user_id,
          chatsession_id: phaseinstance["chatsession_id"]
        }
      }).then( function(userxchatsession) {
        if (userxchatsession == null) {
          console.error("[GetPhase3ApprovalForUserAndStudentTeam] No UserXChatSession record found for user_id " + user_id + " and chatsession_id " + phaseinstance["chatsession_id"]);
          return null;
        }
        else {
          console.log("[GetPhase3ApprovalForUserAndStudentTeam] Got userxchatsession: " + JSON.stringify(userxchatsession) + ", returning " + userxchatsession["approved_phase_state"]);
          return userxchatsession["approved_phase_state"];
        }
      });
    }
  });
}

function GetPhase3Approvals(phase3team_id) {
  phase3team_id = MaybeIDFromInstance(phase3team_id);
  return GetUsersInPhase3Team(phase3team_id).then( function(users) {
    return MapPromise(
      GetPhase3ApprovalForUserAndStudentTeam,
      users,
      phase3team_id
    ).then( function(userApprovedPhaseStates) {
      return MapPromise(
        GetUserOnlineForUserAndStudentTeamPhasePair,
        users,
        [phase3team_id, 3]
      ).then( function(userOnlineBools) {
        var approvalStates = [];
        for (var i = 0; i < userApprovedPhaseStates.length; i++) {
          approvalStates.push({
            username: users[i]["username"], // MapPromise preserves ordering
            is_user_online: userOnlineBools[i],
            approved: userApprovedPhaseStates[i] // integer, -1 / 1
          });
        }
        return approvalStates;
      });
    });
  });
  /*
  [
    {
    username: string,
    approved: integer (-1 / 1)
    },
    ...
  ]
  */
}
exportFunctions["GetPhase3Approvals"] = GetPhase3Approvals;

function SetPhase3SlotSDEChoice(slot_id, sde_id) {
  slot_id = MaybeIDFromInstance(slot_id);
  sde_id = MaybeIDFromInstance(sde_id);
  return GetOrCreatePhase3ChoiceForSlot(slot_id).then( function(phase3Choice) {
    if (phase3Choice == null) {
      console.error("[SetPhase3SlotSDEChoice] No Phase3Choice record found for slot_id: " + slot_id + "; returning null.");
      return null;
    }
    else {
      phase3Choice.schooldesignelement_id_unsafe = sde_id;
      return phase3Choice.save().then( function(phase3Choice) {
        return {
          phase3slot_id: slot_id,
          schooldesignelement_id_unsafe: phase3Choice.schooldesignelement_id_unsafe
        };
      });
    }
  });
}
exportFunctions["SetPhase3SlotSDEChoice"] = SetPhase3SlotSDEChoice;

function SetPhase3ApprovalForUserAndStudentTeam(user_id, phase3team_id, approved) {
  user_id = MaybeIDFromInstance(user_id);
  studentteam_id = MaybeIDFromInstance(studentteam_id);
  return GetPhaseInstanceForTeamAndPhase(studentteam_id, 3).then( function(phaseinstance) {
    return UserXChatSession.findOne({
      where: {
        user_id: user_id,
        chatsession_id: phaseinstance["chatsession_id"]
      }
    }).then( function(userxchatsession) {
      userxchatsession.approved_phase_state = approved_phase_state;
      return userxchatsession.save();
    });
  });
}
exportFunctions["SetPhase3ApprovalForUserAndStudentTeam"] = SetPhase3ApprovalForUserAndStudentTeam;

function GetUserXChatSessionForUserAndTeamPhasePair(user_id, teamPhasePair) {
  return GetUserXChatSessionForUserAndTeamAndPhase(user_id, teamPhasePair[0], teamPhasePair[1]);
}

function GetTeachersForStudentTeam(studentTeamInstance) {
  if (studentTeamInstance["is_phase3_team"]) {
    return GetStudentTeamsForPhase3Team(studentTeamInstance).then( function(studentTeams) {
      if (studentTeams.length == 0) {
        console.error("No StudentTeam records found for Phase3Team id: " + studentTeamInstance["id"] + "; returning null");
        return null;
      }
      return TeacherPrivelege.findAll({
        where: {
          gamesession_id: studentTeams[0]["gamesession_id"]
        }
      }).then( function(teacherprivileges) {
        return InstancesFromIDsPromise(GetColumnsFromInstances(teacherprivileges, "user_id"), User);
      });
    });
  }
  else {
    return TeacherPrivelege.findAll({
      where: {
        gamesession_id: studentTeamInstance["gamesession_id"]
      }
    }).then( function(teacherprivileges) {
      return InstancesFromIDsPromise(GetColumnsFromInstances(teacherprivileges, "user_id"), User);
    });
  }
}

function GetTeacherOnlineStatusForTeamAndPhase(studentTeamInstance, phase) {
  return new Promise( function(resolve, reject) {
    GetTeachersForStudentTeam(studentTeamInstance).then( function(teacherUsers) {
      MapPromise(
        GetUserXChatSessionForUserAndTeamPhasePair,
        teacherUsers,
        [studentTeamInstance, phase]
      ).then( function(teacher_userxchatsessions) {
        var teacherOnlineStatus = [];
        for (var i = 0; i < teacher_userxchatsessions.length; i++) {
          teacherOnlineStatus.push({
            username: teacherUsers[i].username,
            userIsTeacher: true,
            user_is_online: teacher_userxchatsessions[i]["is_user_online"]
          });
        }
        resolve(teacherOnlineStatus);
      });
    });
  });
}
exportFunctions["GetTeacherOnlineStatusForTeamAndPhase"] = GetTeacherOnlineStatusForTeamAndPhase;

module.exports = exportFunctions;
