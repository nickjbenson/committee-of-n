// app/routes/phase1.js
// Routes and methods specific to Phase 1.

var Query = require("../query.js");

module.exports = function(app) {

  app.get("/session/:sessionName/phase1/manage", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    res.redirect("/session/" + req.gamesession.name + "/manage");
  });

  app.get("/session/:sessionName/phase1/progress", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    Query.CreatePhase2DataFromPhase1Data(req.gamesession).then( function(phase2choices) {
      if (phase2choices.length == 0) {
        console.warn("GET phase1/progress] For gamesession " + req.gamesession.name + ", unable to  create Phase2Choice records. Are there non-design-feature-empty SDEs associated with the session via its StudentTeams?");
        req.flash("redirectMessage", "Unable to create phase 2 data from phase 1.");
        res.redirect("/dashboard");
      }
      else {
        Query.SetGameSessionPhase(req.gamesession, 2).then( function() {
          console.log("GET phase1/progress] For gamesession " + req.gamesession.name + ", successfully created " + phase2choices + " Phase2Choice records.");
          req.flash("redirectMessage", "Successfully progressed " + req.gamesession.name + " to phase 2!");
          res.redirect("/dashboard");
        });
      }
    });
  });

  app.get("/session/:sessionName/phase1/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    Query.GetStudentTeamForUserInGameSession(req.user.id, req.gamesession).then( function(studentteam) {
      if (studentteam == null) {
        req.flash("redirectMessage", "You are not a participant in the requested session. If you are a teacher for the session, join a specific team's phase instance by clicking on their team in the session summary.");
        res.redirect("/dashboard");
      }
      else {
        res.redirect("/session/"+req.gamesession.name+"/team/"+studentteam.id+"/phase1/view");
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase1/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    res.render("phase1", {
      username: req.user.get("username"),
      userIsTeacherForSession: req.userIsTeacherForSession ? true : false
    });
  });

  /** AJAX Methods **/

  app.get("/session/:sessionName/team/:studentTeamID/phase1/handdata", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase1 (ajax) GET handdata] Don't have a student team ID; can't GET handdata.");
      res.send({
        phase1hands: []
      });
    }
    else {
      Query.GetSchoolDesignElementsForStudentTeam(studentTeam).then( function(schoolDesignElements) {
        if (schoolDesignElements == null) {
          console.error("[phase1 (ajax) GET handdata] Unable to get schoolDesignElements from the DB.");
          return null;
        }
        // The query doesn't return SDEs in a consistent order, so they
        // are sorted client-side in phase1.ejs for consistency
        // (because the view assigns them indices 1-8).
        var handJSONs = [];
        for (var i = 0; i < schoolDesignElements.length; i++) {
          Query.GetHandJSONFromSchoolDesignElement(schoolDesignElements[i]).then( function(handJSON) {
            handJSONs.push(handJSON);
            if (handJSONs.length == schoolDesignElements.length) {
              res.send({
                phase1hands: handJSONs
              });
            }
          });
        }
      });
    }
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase1/getChatHistory", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase1 (ajax) GET getChatHistory] Don't have a student team ID; can't GET chat history.");
      res.send({});
    }
    else {
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 1).then( function(phaseinstance) {
        Query.GetChatHistoryForPhaseInstance(phaseinstance).then( function(chatMessages) {
          res.send(chatMessages);
        });
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase1/pushChatMessage", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase1 (ajax) POST pushChatMessage] Don't have a student team ID; can't POST chat message.");
      res.send({});
    }
    else {
      var username = req.user.get("username");
      var chat_color = req.user.get("chat_color");
      var message = req.body.message;
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 1).then( function(phaseinstance) {
        Query.PushChatMessageForPhaseInstance(phaseinstance, username, chat_color, message).then( function(chatmessage) {
          if (chatmessage == null) {
            res.send({
              error: "Unable to create a new chat message in the database."
            })
          }
          else {
            res.send(chatmessage);
          }
        });
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase1/saveFeatures", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var sde_id = req.body.sde_id;
    var features = req.body.features;
    var title = req.body.title;
    Query.SaveSchoolDesignElement(sde_id, title, features).then( function(schooldesignelement) {
      if (schooldesignelement == null) {
        res.send({
          error: "Unable to save SchoolDesignElement with id: " + sde_id
        });
      }
      else {
        res.send(schooldesignelement);
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase1/getApprovalStatus", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase1 (ajax) GET getApprovalStatus] Don't have a student team ID; can't GET approval status.");
      res.send({});
    }
    else {
      Query.GetPhase1SessionApprovalStatusForStudentTeam(studentTeam).then( function(usernameAndApprovalObjects) {
        if (usernameAndApprovalObjects == null) {
          console.error("[phase 1 (ajax) GET getApprovalStatus] Unable to retrieve any user approval status records from the DB. Returning an empty list...");
          res.send({
            usernameAndApprovalObjects: []
          });
        }
        else {
          Query.GetTeacherOnlineStatusForTeamAndPhase(studentTeam, 1).then( function(teacherOnlineStatus) {
            console.log("[phase 1 (ajax) GET getApprovalStatus] Returned from GetPhase1SessionApprovalStatusForStudentTeam, sending this object: " + JSON.stringify({
              usernameAndApprovalObjects: usernameAndApprovalObjects,
              teacherOnlineStatus: teacherOnlineStatus
            }));
            res.send({
              usernameAndApprovalObjects: usernameAndApprovalObjects,
              teacherOnlineStatus: teacherOnlineStatus
            });
          });
        }
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase1/setApproval", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    var sde_id = req.body.sde_id;
    var approved = req.body.approved;
    if (req.userIsTeacherForSession) {
      Query.SetTeacherApprovedForSDE(sde_id, approved).then( function(schooldesignelement) {
        if (schooldesignelement == null) {
          console.error("[phase1 (ajax) POST setApproval] Unable to find SchoolDesignElement with id " + sde_id);
          res.send({
            error: true,
            message: "[phase1 (ajax) POST setApproval] Unable to find SchoolDesignElement with id " + sde_id
          });
        }
        else {
          res.send({
            username: req.user["username"],
            userWasTeacher: true,
            sde_id: sde_id,
            setApprovedAs: schooldesignelement["teacher_approved"]
          });
        }
      });
    }
    else {
      Query.SetSDEApprovalForUserAndSDE(req.user, sde_id, approved).then( function(sdeApproval) {
        if (sdeApproval == null) {
          console.error("[phase1 (ajax) POST setApproval] Unable to set or create sdeApproval record for user " + JSON.stringify(req.user) + " and sde_id " + sde_id);
          res.send({
            error: true,
            message: "Unable to set or create sdeApproval record for user " + JSON.stringify(req.user) + " and sde_id " + sde_id
          });
        }
        else {
          console.log("[phase 1 (ajax) POST setApproval]] Returned from SetSDEApprovalForUserAndSDE, sending this object: " + JSON.stringify({
            sdeApproval: sdeApproval
          }));
          res.send({
            userWasTeacher: false,
            sde_id: sde_id,
            setApprovedAs: sdeApproval["approved"]
          });
        }
      });
    }
  });

  /*

  app.get("/session/:sessionName/team/:studentTeamID/phase1/getSocketSession", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    89u5y89&*( %Y$&#*(@Y&*( //TODO FIXME IMPLEMENT
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase1/setSocketSession", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    // var socketSession = req.body.socketSession
    89u5y89&*( %Y$&#*(@Y&*( //TODO FIXME IMPLEMENT
  });

  */

  app.post("/session/:sessionName/team/:studentTeamID/phase1/setUserOnline", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var is_user_online = req.body.is_user_online;
    console.log("[getUserOnline] user_id: " + req.user.id + ", studentteam_id: " + req.studentTeam["id"]);
    Query.SetUserOnlineForUserAndStudentTeamAndPhase(req.user.id, req.studentTeam, 1, is_user_online).then( function(userxchatsession) {
      res.send({
        userxchatsession: userxchatsession
      });
    });
  });

}
