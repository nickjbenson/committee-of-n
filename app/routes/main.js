// app/routes/index.js
// Entry point for all application routing. The server should only require this
// file, and it will load all of the other routes necessary for the application.
// Also includes routes for global actions such as the home page and any other
// generic, non-user-specific pages.


module.exports = function(app, passport) {

  /** Route Loading **/

  require("./login.js")(app, passport);
  require("./dashboard.js")(app);
  require("./phase1.js")(app);
  require("./phase2.js")(app);
  require("./phase3.js")(app);


  /** Generic Routes **/

  // In the future, Committee of N may want a generic home page.
  app.get("/", function(req, res) {
    if (app.isLoggedIn(req)) {
      res.redirect("/dashboard");
    }
    else {
      res.redirect("/login");
    }
  });

  // Similarly, it will probably want an about page.
  app.get("/about", function(req, res) {
    res.redirect("/");
  });

}
