// app/routes/phase3.js
// Routes and methods specific to Phase 3.

var Query = require('../query.js');

module.exports = function(app) {

  app.get("/session/:sessionName/phase3/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    Query.GetStudentTeamForUserInGameSession(req.user.id, req.gamesession).then( function(studentteam) {
      if (studentteam == null) {
        req.flash("redirectMessage", "You are not a participant in the requested session. If you are a teacher for the session, join a specific team's phase instance by clicking on their team in the session summary.");
        res.redirect("/dashboard");
      }
      else {
        res.redirect("/session/"+req.gamesession.name+"/team/"+studentteam.id+"/phase3/view");
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase3/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (!studentTeam["is_phase3_team"]) {
      Query.GetPhase3TeamIDForStudentTeam(studentTeam).then( function(phase3TeamID) {
        res.redirect("/session/"+req.gamesession.name+"/team/"+phase3TeamID+"/phase3/view");
      });
    }
    else {
      res.render("phase3", {
        username: req.user["username"],
        userIsTeacherForSession: req.userIsTeacherForSession ? true : false
      });
    }
  });

  /** AJAX Methods **/

  app.get("/session/:sessionName/team/:studentTeamID/phase3/getSDEData", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var phase3Team = req.studentTeam;
    Query.GetPhase3SDEDataForTeam(phase3Team).then( function(sdeData) {
      if (sdeData == null) {
        console.error("[phase3 (ajax) GET getSDEData] Unable to get SDE data for phase3Team id " + phase3Team["id"]);
        res.send({
          error: true,
          message: "Unable to get SDE data for phase3Team id " + phase3Team["id"] + " in phase 3."
        });
      }
      else {
        console.log("[phase3 (ajax) GET getSDEData] Returning " + JSON.stringify(sdeData));
        res.send(sdeData);
      }
    });
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase3/setSlotSDEChoice", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    var slot_id = req.body.slot_id;
    var sde_id = req.body.sde_id;
    Query.SetPhase3SlotSDEChoice(slot_id, sde_id).then( function(slotIDAndChoice) {
      if (slotIDAndChoice == null) {
        res.send({
          error: true,
          message: "No Phase3Slot found for slot_id: " + slot_id
        });
      }
      else {
        res.send({
          slot_id: slotIDAndChoice["phase3slot_id"],
          chosen_sde_id: slotIDAndChoice["schooldesignelement_id_unsafe"]
        });
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase3/getPhaseApprovals", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    Query.GetPhase3Approvals(studentTeam).then( function(userApprovals) {
      Query.GetTeacherOnlineStatusForTeamAndPhase(studentTeam, 3).then( function(teacherOnlineStatus) {
        res.send({
          userApprovals: userApprovals,
          teacherOnlineStatus: teacherOnlineStatus
        });
      });
    });
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase3/setPhaseApproval", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    var approved = req.body.approved;
    if (req.userIsTeacherForSession) {
      // Set teacher_approved for this PhaseInstance
      if (approved === true || approved === false) {
        console.error("[phase3 (ajax) POST setPhaseApproval] Type mismatch! Teacher approval should be an integer. Assuming a true/false mapping of -1/1...");
        approved = approved ? -1 : 1;
      }
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 3).then( function(phaseinstance) {
        if (phaseinstance == null) {
          console.error("[phase3 (ajax) POST setPhaseApproval] Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 3.");
          res.send({
            error: true,
            message: "Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 3."
          });
        }
        else {
          Query.SetTeacherApprovedForPhaseInstance(phaseinstance, approved).then( function(phaseinstance) {
            res.send({
              username: req.user["username"],
              userWasTeacher: true,
              setApprovedAs: phaseinstance["teacher_approved"]
            });
          });
        }
      });
    }
    else {
      // Set user approved_phase_state for this phase via UserXChatSession
      if (approved === true || approved === false) {
        console.error("[phase3 (ajax) POST setPhaseApproval] Type mismatch! User approval should be an integer. Assuming true / false maps to 1 / -1...");
        approved = approved ? -1 : 1;
      }
      Query.SetPhase3ApprovalForUserAndStudentTeam(req.user.id, studentTeam, approved).then( function(userxchatsession) {
        if (userxchatsession == null) {
          console.error("[phase3 (ajax) POST setPhaseApproval] Unable to set phase 3 approval for user id " + req.user.id + ".");
          res.send({
            error: true,
            message: "Unable to set phase 3 approval for user id " + req.user.id + "."
          });
        }
        else {
          res.send({
            username: req.user["username"],
            userWasTeacher: false,
            setApprovedAs: userxchatsession["approved_phase_state"]
          });
        }
      });
    }
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase3/getChatHistory", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 3).then( function(phaseinstance) {
      Query.GetChatHistoryForPhaseInstance(phaseinstance).then( function(chatMessages) {
        res.send(chatMessages);
      });
    });
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase3/pushChatMessage", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase3 (ajax) POST pushChatMessage] Don't have a student team ID; can't POST chat message.");
      res.send({});
    }
    else {
      var username = req.user.get("username");
      var chat_color = req.user.get("chat_color");
      var message = req.body.message;
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 3).then( function(phaseinstance) {
        Query.PushChatMessageForPhaseInstance(phaseinstance, username, chat_color, message).then( function(chatmessage) {
          if (chatmessage == null) {
            res.send({
              error: "Unable to create a new chat message in the database."
            })
          }
          else {
            res.send(chatmessage);
          }
        });
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase3/setUserOnline", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var is_user_online = req.body.is_user_online;
    console.log("[getUserOnline] user_id: " + req.user.id + ", studentteam_id: " + req.studentTeam["id"]);
    Query.SetUserOnlineForUserAndStudentTeamAndPhase(req.user.id, req.studentTeam, 3, is_user_online).then( function(userxchatsession) {
      res.send({
        userxchatsession: userxchatsession
      });
    });
  });

};
