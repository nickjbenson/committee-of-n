// app/routes/login.js
// Routes for user login/logout/registration/etc., and adds helper methods to
// the app object for login-related functionality (e.g. app.isLoggedIn())

module.exports = function(app, passport) {

  // TODO: Debug only, populating users; delete soon. -NB
  var User = require("../../db/models/user.js");
  app.get("/debug_populate_users", function(req, res) {
    var letters = "abcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < letters.length; i++) {
      var letter = letters[i];
      var newUser = User.findOrCreate({
        where: {
          username: letter
        },
        defaults: {
          username: letter,
          password: User.generateHash(letter),
          email: "",
          chat_color: 0x000000
        }
      });
    }
    res.send({ message: "OK, alphabetical users populated." });
  });


  /** Routes **/

  app.get("/login", function(req, res) {
    var message = req.flash("successMessage");
    var success = true;
    if (message === undefined || message == "") {
      message = req.flash("redirectMessage");
      success = false;
    }
    res.render("login", {
      message: message,
      success: success
    });
  });
  app.post("/login", function(req, res, next) {
    passport.authenticate("local-login", function(err, user, info) {
      if (err) {
        console.log("[login] Error: " + err);
        return next(err);
      }
      else if (!user) {
        if (info.message !== undefined) {
          if (info.message == "Missing credentials") {
            req.flash("redirectMessage", "Username and password are both required.");
          }
          else {
            req.flash("redirectMessage", "Unhandled passport error: " + info.message);
          }
        }
        else if (info != "") {
          req.flash("redirectMessage", info);
        }
        return res.redirect("/login");
      }
      req.logIn(user, function(err) {
        if (err) {
          console.log("[POST /login, logIn()] Error: " + err);
          return next(err);
        }
        return res.redirect("/");
      });
    }) (req, res, next);
  });

  app.get("/logout", function(req, res) {
    if (app.isLoggedIn(req)) {
      req.flash("redirectMessage", "You have successfully logged out.");
      req.logout();
      res.redirect("/");
    }
    else {
      req.flash("redirectMessage", "No need to log out; you are not logged in.");
      res.redirect("/");
    }
  });

  app.get("/register", function(req, res) {
    res.render("register", {
      message: req.flash("redirectMessage")
    });
  });
  app.post("/register", function(req, res, next) {
    passport.authenticate("local-signup", function(err, user, info) {
      if (err) {
        console.log("[login] Error: " + err);
        return next(err);
      }
      else if (!user) {
        if (info.message !== undefined) {
          if (info.message == "Missing credentials") {
            req.flash("redirectMessage", "Username and password are both required.");
          }
          else {
            req.flash("redirectMessage", "Unhandled passport error: " + info.message);
          }
        }
        else if (info != "") {
          req.flash("redirectMessage", info);
        }
        return res.redirect("/register");
      }
      req.logIn(user, function(err) {
        if (err) {
          console.log("[POST /register, logIn()] Error: " + err);
          return next(err);
        }
        user.get("username").then( function(username) {
          req.flash("successMessage", "Successfully registered user " + username + "! Please log in.");
          return res.redirect("/login");
        });
      });
    }) (req, res, next);
  });


  /** Methods **/

  app.isLoggedIn = function(req) {
    return req.isAuthenticated();
  }


  /** Middleware **/

  app.confirmLoggedIn = function(req, res, next) {
    if (app.isLoggedIn(req)) {
      next();
    }
    else {
      req.flash("redirectMessage", "Please log in first.");
      res.redirect("/login");
    }
  }

};
