// app/routes/phase2.js
// Routes and methods specific to Phase 2.

var Query = require('../query.js');

module.exports = function(app) {

  app.get("/session/:sessionName/phase2/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    Query.GetStudentTeamForUserInGameSession(req.user.id, req.gamesession).then( function(studentteam) {
      if (studentteam == null) {
        req.flash("redirectMessage", "You are not a participant in the requested session. If you are a teacher for the session, join a specific team's phase instance by clicking on their team in the session summary.");
        res.redirect("/dashboard");
      }
      else {
        res.redirect("/session/"+req.gamesession.name+"/team/"+studentteam.id+"/phase2/view");
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase2/view", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    res.render("phase2", {
      username: req.user["username"],
      userIsTeacherForSession: req.userIsTeacherForSession ? true : false
    });
  });

  app.get("/session/:sessionName/phase2/manage", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    res.redirect("/session/" + req.gamesession.name + "/manage");
  });

  app.get("/session/:sessionName/phase2/progress", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    res.render("phase2to3", {
      username: req.user["username"]
    });
  });

  /** AJAX Methods **/

  app.get("/session/:sessionName/team/:studentTeamID/phase2/handdata", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase2 (ajax) GET handdata] Don't have a student team ID; can't GET handdata.");
      res.send({
        phaseApprovedByTeacher: false,
        phase2hands: []
      });
    }
    else {
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 2).then( function(phaseInstance) {
        if (phaseInstance == null) {
          console.error("[phase2 (ajax) POST setPhaseApproval] Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 2.");
          res.send({
            error: true,
            message: "Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 2."
          });
        }
        else {
          Query.GetPhase2HandDataForStudentTeam(studentTeam).then( function(handJSONs) {
            console.log("[phase2 (ajax) GET handdata] Returning object: " + JSON.stringify({
              phaseApprovedByTeacher: phaseInstance["teacher_approved"],
              phase2hands: handJSONs
            }));
            res.send({
              phaseApprovedByTeacher: phaseInstance["teacher_approved"],
              phase2hands: handJSONs
            });
          });
        }
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase2/sdeChoice", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    var sde_id = req.body.sde_id;
    var chosen_for_school = req.body.chosen_for_school;
    console.log("[phase2 (ajax) POST sdeChoice] Got SDE id: " + sde_id + ", chosen_for_school: " + chosen_for_school);
    Query.SetPhase2ChoiceForStudentTeamAndSDE(studentTeam, sde_id, chosen_for_school).then( function(phase2choice) {
      if (phase2choice == null) {
        res.send({
          message: "Unable to set choice for phase 2; No Phase2Choice record found for studentteam_id " + studentTeam["id"] + " and sde_id " + sde_id + ". Was Phase 2 initialized properly?"
        });
      }
      else {
        res.send(phase2choice);
      }
    });
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase2/getPhaseApprovals", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    Query.GetPhase2Approvals(studentTeam).then( function(userApprovals) {
      Query.GetTeacherOnlineStatusForTeamAndPhase(studentTeam, 2).then( function(teacherOnlineStatus) {
        res.send({
          userApprovals: userApprovals,
          teacherOnlineStatus: teacherOnlineStatus
        });
      });
    });
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase2/setPhaseApproval", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    var approved = req.body.approved;
    if (req.userIsTeacherForSession) {
      // Set teacher_approved for this PhaseInstance
      if (approved === true || approved === false) {
        console.error("[phase2 (ajax) POST setPhaseApproval] Type mismatch! Teacher approval should be an integer. Assuming a true/false mapping of -1/1...");
        approved = approved ? -1 : 1;
      }
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 2).then( function(phaseinstance) {
        if (phaseinstance == null) {
          console.error("[phase2 (ajax) POST setPhaseApproval] Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 2.");
          res.send({
            error: true,
            message: "Unable to get phaseinstance for studentteam_id " + studentTeam["id"] + " in phase 2."
          });
        }
        else {
          Query.SetTeacherApprovedForPhaseInstance(phaseinstance, approved).then( function(phaseinstance) {
            res.send({
              username: req.user["username"],
              userWasTeacher: true,
              setApprovedAs: phaseinstance["teacher_approved"]
            });
          });
        }
      });
    }
    else {
      // Set user approved_phase_state for this phase via UserXChatSession
      if (approved === true || approved === false) {
        console.error("[phase2 (ajax) POST setPhaseApproval] Type mismatch! User approval should be an integer. Assuming true / false maps to 1 / -1...");
        approved = approved ? -1 : 1;
      }
      Query.SetPhase2ApprovalForUserAndStudentTeam(req.user.id, studentTeam, approved).then( function(userxchatsession) {
        if (userxchatsession == null) {
          console.error("[phase2 (ajax) POST setPhaseApproval] Unable to set phase 2 approval for user id " + req.user.id + ".");
          res.send({
            error: true,
            message: "Unable to set phase 2 approval for user id " + req.user.id + "."
          });
        }
        else {
          res.send({
            username: req.user["username"],
            userWasTeacher: false,
            setApprovedAs: userxchatsession["approved_phase_state"]
          });
        }
      });
    }
  });

  app.get("/session/:sessionName/phase2/getTeams", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    Query.GetTeamSummaryForSession(req.gamesession).then( function(teamSummary) {
      console.log("[phase2 (ajax) GET getTeams] Returning teamSummary: " + JSON.stringify(teamSummary));
      res.send({
        studentTeams: teamSummary
      });
    });
  });

  // This AJAX route sets the game session's phase from 2 to 3 if it completes successfullly.
  app.post("/session/:sessionName/phase2/setPhase3Teams", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    var phase3TeamsArray = req.body.phase3Teams;
    console.log("[phase2 (ajax) POST setPhase3Teams] Got phase3TeamsArray: " + JSON.stringify(phase3TeamsArray));
    if (phase3TeamsArray.length == 0) {
      res.send({
        error: true,
        message: "No phase 3 team specification submitted as data; unable to create phase 3 teams."
      });
    }
    else {
      Query.CreateStudentTeams(req.gamesession, phase3TeamsArray.length, true).then( function(phase3Teams) {
        Query.MapPromise( // preserves ordering
          Query.GetUsersFromUsernames,
          phase3TeamsArray
        ).then( function(userInstanceArrays) {
          console.log("[phase2 (ajax) POST setPhase3Teams] From phase3TeamsArray got userInstanceArrays: " + JSON.stringify(userInstanceArrays));
          Query.GetStudentTeamArraysFromUserArraysAndSession(userInstanceArrays, req.gamesession).then( function(studentTeamArrays) {
            console.log("[phase2 (ajax) POST setPhase3Teams] Got studentTeamArrays: " + JSON.stringify(studentTeamArrays));
            Query.DoubleMapPromise( // relies on preserved ordering
              Query.CreatePhase3MergedMembershipsForPhase3TeamAndStudentTeams,
              phase3Teams,
              studentTeamArrays
            ).then( function(phase3MergedMembershipArrays) {
              console.log("[phase2 (ajax) POST setPhase3Teams] Got merged memberships: " + JSON.stringify(phase3MergedMembershipArrays));
              // create N Phase3Slots per phase3team, one for each gray card
              console.log("[phase2 (ajax) POST setPhase3Teams] Preparing phase 3 slots for phase3Teams: " + JSON.stringify(phase3Teams));
              Query.PreparePhase3SlotsForPhase3Teams(phase3Teams).then( function(phase3SlotArrays) {
                console.log("[phase2 (ajax) POST setPhase3Teams] Got phase3SlotArrays: " + JSON.stringify(phase3SlotArrays));
                Query.PreparePhase3Instances(phase3Teams).then( function(counts) {
                  Query.SetGameSessionPhase(req.gamesession, 3).then( function() {
                    // calculate and return counts for verification
                    var responseObj = {
                      message: "Successfully progressed " + req.gamesession.name + " to phase 3!",
                      createdPhase3Teams: phase3Teams.length,
                      createdPhase3MergedMembershipArrays: phase3MergedMembershipArrays.length,
                      createdPhase3SlotArrays: phase3SlotArrays.length,
                      phase3SlotArrayLength: phase3SlotArrays[0].length,
                      numPhaseInstances: counts.numPhaseInstances,
                      numChatSessions: counts.numChatSessions,
                      numUserXChatSessions: counts.numUserXChatSessions,
                      numTeacherUserXChatSessions: counts.numTeacherUserXChatSessions
                    }
                    console.log("[phase2 (ajax) POST setPhase3Teams] Counts: " + JSON.stringify(responseObj));
                    res.send(responseObj);
                  });
                });
              });
            });
          });
        });
      });
    }
  });

  app.get("/session/:sessionName/team/:studentTeamID/phase2/getChatHistory", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 2).then( function(phaseinstance) {
      Query.GetChatHistoryForPhaseInstance(phaseinstance).then( function(chatMessages) {
        res.send(chatMessages);
      });
    });
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase2/pushChatMessage", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var studentTeam = req.studentTeam;
    if (studentTeam == null) {
      console.error("[phase2 (ajax) POST pushChatMessage] Don't have a student team ID; can't POST chat message.");
      res.send({});
    }
    else {
      var username = req.user.get("username");
      var chat_color = req.user.get("chat_color");
      var message = req.body.message;
      Query.GetPhaseInstanceForTeamAndPhase(studentTeam, 2).then( function(phaseinstance) {
        Query.PushChatMessageForPhaseInstance(phaseinstance, username, chat_color, message).then( function(chatmessage) {
          if (chatmessage == null) {
            res.send({
              error: "Unable to create a new chat message in the database."
            })
          }
          else {
            res.send(chatmessage);
          }
        });
      });
    }
  });

  app.post("/session/:sessionName/team/:studentTeamID/phase2/setUserOnline", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmURLStudentTeamIDExists, app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession, function(req, res) {
    var is_user_online = req.body.is_user_online;
    console.log("[getUserOnline] user_id: " + req.user.id + ", studentteam_id: " + req.studentTeam["id"]);
    Query.SetUserOnlineForUserAndStudentTeamAndPhase(req.user.id, req.studentTeam, 2, is_user_online).then( function(userxchatsession) {
      res.send({
        userxchatsession: userxchatsession
      });
    });
  });

};
