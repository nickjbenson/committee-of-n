// app/routes/dashboard.js
// Routes and functions related to the dashboard and game session management.

var Query = require("../query.js");

module.exports = function(app) {

  /** Middleware **/

  app.confirmURLGameSessionExists = function(req, res, next) {
    var sessionName = req.params.sessionName;
    Query.GetGameSession(sessionName).then( function(gamesession) {
      if (gamesession == null) {
        req.flash("redirectMessage", "No game session exists by that name.");
        res.redirect("/dashboard");
      }
      else {
        req.gamesession = gamesession;
        next();
      }
    });
  };

  app.confirmURLStudentTeamIDExists = function(req, res, next) {
    var studentTeamID = req.params.studentTeamID;
    Query.GetStudentTeamFromID(studentTeamID).then (function(studentTeam) {
      if (studentTeam == null) {
        req.flash("redirectMessage", "The requested Student Team was not found.");
        req.redirect("/dashboard");
      }
      else {
        req.studentTeamID = studentTeam["id"];
        req.studentTeam = studentTeam;
        next();
      }
    });
  }

  app.confirmTeacherPrivelegeForURLSession = function(req, res, next) {
    var user_id = req.user.id;
    var gamesession = req.gamesession;
    Query.CheckTeacherPriveleges(user_id, gamesession).then( function(hasPriveleges) {
      if (!hasPriveleges) {
        req.flash("redirectMessage", "You do not have teacher priveleges for " + gamesession.get("name") + ".");
        res.redirect("/dashboard");
      }
      else {
        req.userIsTeacherForSession = true;
        next();
      }
    });
  };

  app.confirmUserInURLStudentTeamOrTeacherPrivelegeForSession = function(req, res, next) {
    var user_id = req.user.id;
    var gamesession = req.gamesession;
    var studentTeamID = req.params.studentTeamID;
    // teacher?
    Query.CheckTeacherPriveleges(user_id, gamesession).then( function(hasPriveleges) {
      req.userIsTeacherForSession = hasPriveleges;
      // member of specified student team?
      if (!req.studentTeam["is_phase3_team"]) {
        Query.CheckUserInTeam(user_id, studentTeamID).then( function(userxstudentteam) {
          req.userInStudentTeamForSession = userxstudentteam != null;
          if (!req.userInStudentTeamForSession && !hasPriveleges) {
            req.flash("redirectMessage", "You do not have access to team ID " + studentTeamID + ".");
            res.redirect("/dashboard");
          }
          next();
        });
      }
      else {
        Query.CheckUserInPhase3Team(user_id, studentTeamID).then( function(isUserInPhase3Team) {
          req.userInStudentTeamForSession = isUserInPhase3Team;
          if (!req.userInStudentTeamForSession && !hasPriveleges) {
            req.flash("redirectMessage", "You do not have access to team ID " + studentTeamID + ".");
            res.redirect("/dashboard");
          }
          next();
        });
      }
    });
  }

  /** Routes **/

  app.get("/dashboard", app.confirmLoggedIn, function(req, res) {
    var username = req.user.get("username");
    Query.SessionsOverviewFromUsername(username).then( function(gamesessionsOverviewObj) {
      console.log("[GET /dashboard] rendering page with obj: " + JSON.stringify(gamesessionsOverviewObj));
      res.render("dashboard", {
        message: req.flash("redirectMessage"),
        username: gamesessionsOverviewObj.username,
        gamesessionsTeacherOnly: gamesessionsOverviewObj.gamesessionsTeacherOnly,
        gamesessionsParticipating: gamesessionsOverviewObj.gamesessionsParticipating
      });
    });
  });

  app.get("/session/:sessionName", app.confirmLoggedIn, app.confirmURLGameSessionExists, function(req, res) {
    var sessionName = req.params.sessionName;
    res.redirect("/session/" + sessionName + "/phase" + req.gamesession.phase + "/view");
  });

  app.get("/session/:sessionName/manage", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    var username = req.user.get("username");
    res.render("managesession", {
      username: username,
      sessionName: req.gamesession.name
    });
  });


  /** AJAX Routes **/

  app.post("/session/:sessionName/create", app.confirmLoggedIn, function(req, res) {
    var sessionName = req.params.sessionName;
    var phase = 1; // sessions begin in phase 1
    Query.CreateGameSession(sessionName, phase).spread( function(gamesession, error) {
      if (error != null) {
        res.send({
          success: false,
          sessionName: sessionName,
          message: error
        });
      }
      else {
        var user_id = req.user.id;
        Query.GiveTeacherPriveleges(user_id, gamesession).then( function(teacherprivelege) {
          res.send({
            success: true,
            sessionName: gamesession.get("name"),
            message: ""
          });
        });
      }
    });
  });

  app.post("/isUser/:username", app.confirmLoggedIn, function(req, res) {
    var username = req.params.username;
    var usernameFieldIndex = req.body.usernameFieldIndex;
    Query.IsUser(username).then( function(isUser) {
      res.send({
        username: username,
        isUser: isUser,
        usernameFieldIndex: usernameFieldIndex
      });
    });
  });

  // This setTeams route applies to Phase 1 and Phase 2, but a separate routes
  // exists for setting the phase 3 teams as a part of the transition from Phase 2
  // to Phase 3. See phase2.js for the transitional setPhase3Teams route.
  app.post("/session/:sessionName/setTeams", app.confirmLoggedIn, app.confirmURLGameSessionExists, app.confirmTeacherPrivelegeForURLSession, function(req, res) {
    var studentTeamsArray = req.body.studentTeams;
    Query.CreateStudentTeams(req.gamesession, studentTeamsArray.length, false).then( function(studentTeams) {
      var userInstanceArrays = [];
      for (var i = 0; i < studentTeams.length; i++) {
        Query.GetUsersFromUsernames(studentTeamsArray[i]).then( function(users) {
          userInstanceArrays.push(users);
          if (userInstanceArrays.length == studentTeams.length) {
            var teamsReady = 0;
            for (var i = 0; i < userInstanceArrays.length; i++) {
              var usersInTeam = userInstanceArrays[i];
              var studentTeam = studentTeams[i];
              Query.AddUsersToStudentTeam(usersInTeam, studentTeam).then (function(userxstudentteams) {
                teamsReady++;
                if (teamsReady == studentTeams.length) {
                  console.log("[setTeams] Preparing phases using studentTeams.");
                  Query.PreparePhase1And2Instances(studentTeams).then( function(counts) {
                    var responseObj = {
                      message: "Created " + studentTeams.length + " teams.",
                      studentTeams: studentTeams,
                      createdPhaseInstances: counts.numPhaseInstances,
                      createdChatSessions: counts.numChatSessions,
                      createdUserXChatSessions: counts.numUserXChatSessions
                    };
                    // One more thing! For each student team, create SDEs (hand data).
                    // Doing it here makes loading up the hand data faster and more reliable when entering a session.
                    Query.PrepareSchoolDesignElementsForStudentTeams(studentTeams).then( function(schooldesignelement_lists) {
                      responseObj.createdSchoolDesignElements = schooldesignelement_lists.length * 8; // Note: hard-coded, always create 8 SDEs per student team.
                      res.send(responseObj);
                    });
                  });
                }
              });
            }
          }
        });
      }
    });
  });
};
