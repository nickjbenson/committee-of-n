# Committee of N #

![committee_of_n_screenshot.png](https://bytebucket.org/nickjbenson/committee-of-n/raw/2247c843206df3acd750c090881f17061822a844/committee_of_n_screenshot.png)

### Clone the repo. ###

```
#!bash
git clone https://bitbucket.org/nickjbenson/committee-of-n.git
```

### Launch the game server with npm run deploy. ###

    npm run deploy

The server runs by default on localhost:3000. Start the server and point your browser there to get in the game (only tested on Chrome).

### Make sure you have your PostgreSQL database configured. ###

You'll need a Postgres database running on your local machine (default port: 5432) for the server to run correctly. Committee of N was originally developed with Postgres.app as a local database. Node.js will need a user capable of creating other users, so make sure you get permission for that if you're spinning up a new database. The database details can be configured in config/database.js:

```
#!javascript
var postgres_db_config = 'postgres://node_dev_user:node_dev_pass@localhost:5432/condb_dev';
  // postgres://USER:PASS@DB_MACHINE:DB_PORT/DB_NAME
```

### Load example users for quick debugging at /debug_populate_users ###

Visiting ```localhost:3000/debug_populate_users``` will populate the database with username and password combinations such as a:a, b:b, etc., for every letter of the alphabet. Use this to quickly log in and test working with sessions with many users.

### Debug red text in query.js. ###

app/query.js contains the interface layer between the various Sequelize models in db/models/ and the route logic in app/routes/. If you see any red text in the console output, and it's not an obvious syntax error or missing reference, it's probably because something went wrong working with the DB. As of the playtest branch, all supported functionality should work without any database error output.

query.js makes extensive use of promises, and any errors that occur inside a promise often happen completely silently - a problematic route may simply never resolve its promise. Extensive use of logging (or the reject(foo) resolution) is recommended to overcome this when implementing a new feature.

When implementing features that affect the database, it's helpful to use a structural diagram to get a feel for how the tables relate to one another. Check out the image at the bottom of the readme for a diagram of the DB as of the playtest branch.

### Reset the database with npm run annihilate. ###

If you make any structural changes to the DB -- such as adding or removing columns -- you will probably get errors when you try to use those columns unless you reset the database. (This project doesn't support any sort of transition system for porting the DB from one structural state to another.) For development purposes this can be done quickly via ``` npm run annihilate ```. This requires app/purgedb.js, which must have all database models imported to work properly.

### There are a few known issues. ###

- Reverting to a previous phase is not supported.
    - This will require detecting the state change caused by the phase transition and removing the relevant entries from the DB so that phase progression can happen without collisions or duplications. Not difficult, but not yet implemented.
- Managing participants after student teams are first added to a game session is not supported.
    - It's already possible to retrieve the student team data in the same format managesession.ejs sends to the server, but there is no front-end support for retrieving and displaying it to be manipulated, and there is no route to modify a team setup. StudentTeams are extremely important identifiers for game functionality, e.g. they are the identifiers for School Design Elements, so deleting them and recreating entirely new teams would destroy all of the work done for a given session. Instead, SDEs should be transferred over to new teams and StudentTeams should be preserved wherever possible.
    - Username fields are only added to the page and not removed; this can bring the page into a dead-end state where the user can't finish adding users if they make a mistake and want to remove some user entries before submitting. (They must refresh the page to start over again.)
    - Phase 3 transitions are similarly one-directional, but that interface will only appear once for any gamesession (barring phase reverts), so this is less noticeable.
- Visuals on teacher approval indicate (inconsistently) that a given School Design Element or Phase 2 session has been approved by an instructor, but the interface does not actually lock for users.
    - The "teacher has approved" indication only displays for the first School Design Element in phase 1, even if a teacher approves those other School Design Elements.
- If a game is transitioned to the next phase while students are in a phase view, those students will remain in that phase view; they should presumably get redirected to the new phase, or at least get some notification that the game has moved on.
- Phase 2 choice text does not synchronize properly on Phase 2 page load when retrieving that a School Design Element has been chosen.

### What's next? ###

- Fixing / implementing the above bugs / missing features to match users' expectations.
- Support users having usernames as distinct from display names, e.g., proper Firstname Lastname
- Allow teachers to register student users en masse.
- Prevent students from having privileges to create game sessions unless explicitly granted those privileges by a teacher.
- Chat color support exists in the database but does not have front-end support (on registration and when posting chat messages).
- Phase tabs for phases beyond a game session's current phase should be inaccessible from the dashboard.
- Phase 2 choice justification text exists in the database but does not have front-end support.
- Phase 3 choice justification text exists in the database but does not have front-end support.

### Database diagram as of the playtest branch. ###

![db_diagram_060116.png](https://bytebucket.org/nickjbenson/committee-of-n/raw/5fa62abff345eea2cbe7d3c3dfb7c41bc6cbd1d4/db/db_diagram_060116.jpg)