// db/models/userxchatsession.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var User = require("./user.js");
var ChatSession = require("./chatsession.js");

var UserXChatSession = sequelize.define("UserXChatSession", {
  user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  chatsession_id: {
    type: Sequelize.INTEGER,
    references: {
      model: ChatSession,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  is_user_online: Sequelize.BOOLEAN,
  user_socket_session: Sequelize.STRING,
  approved_phase_state: Sequelize.INTEGER, // whether the user has approved the current phase instance. (applicable when in a phase that can be approved in its entirety; phases 2 and 3, not phase 1 (for phase 1 approval, see sdeapproval.js); -1 / 1
  last_message_received_at: Sequelize.DATE // for detecting when the user has new messages in a ChatSession
}, {
  indexes: [ // Index on user_id and chatsession_id.
    {
      fields: ["user_id"]
    },
    {
      fields: ["chatsession_id"]
    }
  ]
});

UserXChatSession.sync();

module.exports = UserXChatSession;
