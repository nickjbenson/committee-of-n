// db/models/teacherprivelege.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var GameSession = require("./gamesession.js");
var User = require("./user.js")

var TeacherPrivelege = sequelize.define("TeacherPrivelege", {
  gamesession_id: {
    type: Sequelize.INTEGER,
    references: {
      model: GameSession,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  }
}, {
  indexes: [ {
    fields: ["user_id"] // Index on user_id.
  } ]
});

TeacherPrivelege.sync();

module.exports = TeacherPrivelege;
