// db/models/phase2choice.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var StudentTeam = require("./studentteam.js");
var SchoolDesignElement = require("./schooldesignelement.js");

var Phase2Choice = sequelize.define("Phase2Choice", {
  studentteam_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  schooldesignelement_id: {
    type: Sequelize.INTEGER,
    references: {
      model: SchoolDesignElement,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  chosen_for_school: Sequelize.BOOLEAN,
  justifications: Sequelize.TEXT
}, {
  indexes: [ { // Index on studentteam_id.
    fields: ["studentteam_id"]
  } ]
});

Phase2Choice.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = Phase2Choice;
