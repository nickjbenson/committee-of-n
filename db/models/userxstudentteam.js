// db/models/userxstudentteam.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var User = require("./user.js");
var StudentTeam = require("./studentteam.js");

var UserXStudentTeam = sequelize.define("UserXStudentTeam", {
  user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  studentteam_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  }
}, {
  indexes: [ // Index on user_id and studentteam_id.
    {
      fields: ["user_id"]
    },
    {
      fields: ["studentteam_id"]
    }
  ]
});

UserXStudentTeam.sync();

module.exports = UserXStudentTeam;
