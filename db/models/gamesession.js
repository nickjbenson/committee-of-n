// db/models/gamesession.js

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');

var GameSession = sequelize.define('GameSession', {
  name: Sequelize.TEXT,
  phase: Sequelize.INTEGER
  // GameSessions aren't really used for anything other than their name and their ID.
  // Everything else is split out into a separate table for modularity. -NB 5/24/16
});

GameSession.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = GameSession;
