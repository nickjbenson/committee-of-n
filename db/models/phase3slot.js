// db/models/phase3slot.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var StudentTeam = require("./studentteam.js");
var Card = require("./card.js");

var Phase3Slot = sequelize.define("Phase3Slot", {
  phase3team_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  graycard_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  }
}, {
  indexes: [ { // Index on Phase 3 StudentTeam FK, phase3team_id.
    fields: ["phase3team_id"]
  } ]
});

Phase3Slot.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = Phase3Slot;
