// /db/models/schooldesignelement.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var StudentTeam = require("./studentteam.js");
var Card = require("./card.js");

var SchoolDesignElement = sequelize.define("SchoolDesignElement", {
  studentteam_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  design_element_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  design_value_1_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  design_value_2_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  design_value_3_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  title: Sequelize.TEXT,
  design_features: Sequelize.TEXT,
  justifications: Sequelize.TEXT,
  teacher_approved: Sequelize.INTEGER // -1 / 0 / 1 <=> teacher wants revision / not approved / approved
}, {
  indexes: [ {
    fields: ["studentteam_id"] // Index on studentteam_id.
  } ]
});

SchoolDesignElement.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = SchoolDesignElement;
