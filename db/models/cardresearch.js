// db/models/cardresearch.js
// A table for research link names and URLs.

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');
var Card = require('./card.js');

var CardResearch = sequelize.define('CardResearch', {
  link_name: Sequelize.TEXT,
  link_url: Sequelize.TEXT,
  card_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Card,
      key: 'id',
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED // PostgreSQL option
    }
  }
}, {
  indexes: [ {
    fields: ['card_id'] // Index on card_id.
  } ]
});

CardResearch.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = CardResearch;
