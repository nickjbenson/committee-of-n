// db/models/cardtype.js

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');

var CardType = sequelize.define('CardType', {
  name: Sequelize.TEXT,
  has_subtype: Sequelize.BOOLEAN,
  subtype_name: Sequelize.TEXT
});

CardType.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = CardType;
