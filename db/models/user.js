// db/models/user.js

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');
var bcrypt = require('bcrypt-nodejs'); // hashed and salted password storage and validation

var User = sequelize.define('User', {
	username: Sequelize.TEXT,
	password: Sequelize.TEXT,
	email: Sequelize.TEXT,
	chat_color: Sequelize.INTEGER, // 0xRRGGBB__ 32-bit integer color encoding (alpha ignored)
});

User.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
User.validPassword = function(user, password) {
	return bcrypt.compareSync(password, user.password);
};

User.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = User;
