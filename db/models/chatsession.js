// db/models/chatsession.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");

var ChatSession = sequelize.define("ChatSession", {
  phase: Sequelize.INTEGER
  // Similar to GameSessions, ChatSessions are used just for a unique ID.
  // Other tables reference specific ChatSessions to provide session-specific
  // functionality. -NB 5/25/15
});

ChatSession.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = ChatSession;
