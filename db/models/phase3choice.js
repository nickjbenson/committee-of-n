// db/models/phase3choice.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var Phase3Slot = require("./phase3slot.js");
var SchoolDesignElement = require("./schooldesignelement.js");

var Phase3Choice = sequelize.define("Phase3Choice", {
  phase3slot_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Phase3Slot,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  schooldesignelement_id_unsafe: Sequelize.INTEGER, // no FK checking for this one!
  justifications: Sequelize.TEXT
}, {
  indexes: [ { // Index on phase3slot_id.
    fields: ["phase3slot_id"]
  } ]
});

Phase3Choice.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = Phase3Choice;
