// db/models/phaseinstance.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var StudentTeam = require("./studentteam.js");
var ChatSession = require("./chatsession.js");

var PhaseInstance = sequelize.define("PhaseInstance", {
  studentteam_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  chatsession_id: {
    type: Sequelize.INTEGER,
    references: {
      model: ChatSession,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  phase: Sequelize.INTEGER,
  teacher_approved: Sequelize.INTEGER // only used for Phases 2 and 3. Possible states: -1 / 0 / 1 (see SchoolDesignElement)
}, {
  indexes: [ { // Index on studentteam_id.
    fields: ["studentteam_id"]
  } ]
});

PhaseInstance.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = PhaseInstance;
