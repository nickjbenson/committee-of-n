// db/models/phase3mergedmembership.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var StudentTeam = require("./studentteam.js");

var Phase3MergedMembership = sequelize.define("Phase3MergedMembership", {
  phase3team_id: { // This is a different, larger StudentTeam formed at the start of Phase 3.
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  studentteam_id: {
    type: Sequelize.INTEGER,
    references: {
      model: StudentTeam,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  }
}, {
  indexes: [ { // Index on phase3team_id (quick searching for the original teams given the phase 3 team).
    fields: ["phase3team_id"]
  } ]
});

Phase3MergedMembership.sync(); // Sync models to DB (create the table if it doesn"t exist)

module.exports = Phase3MergedMembership;
