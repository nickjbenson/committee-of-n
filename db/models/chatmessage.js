// db/models/chatmessage.js

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');
var ChatSession = require('./chatsession.js');

var ChatMessage = sequelize.define('ChatMessage', {
  chatsession_id: {
    type: Sequelize.INTEGER,
    references: {
      model: ChatSession,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  username: Sequelize.TEXT, // just a name instead of a User FK (in case a User is deleted).
  user_color: Sequelize.INTEGER, // see User chat_color column
  message: Sequelize.TEXT,
}, {
  indexes: [ { // Index on chatsession_id.
    fields: ['chatsession_id']
  } ]
});

ChatMessage.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = ChatMessage;
