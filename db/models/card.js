// db/models/card.js

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');
var CardType = require('./cardtype.js');

var Card = sequelize.define('Card', {
  cardtype_id: { // FK to CardType index
    type: Sequelize.INTEGER,
    references: {
      model: CardType,
      key: 'id',
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED // PostgreSQL option, defer FK checks to the end of the transaction.
    }
  },
  title: Sequelize.TEXT,
  description: Sequelize.TEXT,
  quote_text: Sequelize.TEXT,
  quote_author: Sequelize.TEXT
});

Card.sync(); // Sync models to DB (create the table if it doesn't exist)

module.exports = Card;
