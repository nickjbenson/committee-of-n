// db/models/sdeapproval.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var User = require("./user.js");
var SchoolDesignElement = require("./schooldesignelement.js");

var SDEApproval = sequelize.define("SDEApproval", {
  user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  schooldesignelement_id: {
    type: Sequelize.INTEGER,
    references: {
      model: SchoolDesignElement,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  approved: Sequelize.INTEGER, // for students, only -1 / 1.
}, {
  indexes: [ // Index on user_id and chatsession_id.
    {
      fields: ["user_id"]
    },
    {
      fields: ["schooldesignelement_id"]
    }
  ]
});

SDEApproval.sync();

module.exports = SDEApproval;
