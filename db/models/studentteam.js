// db/models/studentteam.js

var Sequelize = require("sequelize");
var sequelize = require("../../config/database.js");
var GameSession = require("./gamesession.js");

var StudentTeam = sequelize.define("StudentTeam", {
  gamesession_id: {
    type: Sequelize.INTEGER,
    references: {
      model: GameSession,
      key: "id",
      deferrable: Sequelize.Deferrable.INITIALLY_DEFERRED
      // PostgreSQL opt, defer FK check to end of transaction.
    }
  },
  is_phase3_team: Sequelize.BOOLEAN // Phase 3 teams are different, larger (merged) StudentTeams.
}, {
  indexes: [ {
    fields: ["gamesession_id"] // Index on gamesession_id.
  } ]
});

StudentTeam.sync();

module.exports = StudentTeam;
