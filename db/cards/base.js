// db/cards/base.js
// Base game card definitions for Committee of N.

var CardType = require('../models/cardtype.js');
var Card = require('../models/card.js');
var CardResearch = require('../models/cardresearch.js');

var Sequelize = require('sequelize');
var sequelize = require('../../config/database.js');


// Only create new cards if they don't
// already exist in the database.
var rowFromModel = {}
function createIfNew(model, instance) {
    if (!rowFromModel[model]) {
        rowFromModel[model] = 1; // starts at row 1
    }
    row = rowFromModel[model];
    model.findOrCreate({
        where: {id: row},
        defaults: instance
    });
    rowFromModel[model] += 1;
}


// Card Types

createIfNew(CardType, {
    name: "Design Element",
    has_subtype: false,
    subtype_name: ""
});

createIfNew(CardType, {
    name: "Design Value",
    has_subtype: true,
    subtype_name: "Theory of Human Intelligence"
});
createIfNew(CardType, {
    name: "Design Value",
    has_subtype: true,
    subtype_name: "Theory of Learning"
});
createIfNew(CardType, {
    name: "Design Value",
    has_subtype: true,
    subtype_name: "Teaching Approach"
});
createIfNew(CardType, {
    name: "Design Value",
    has_subtype: true,
    subtype_name: "Purpose of Schooling"
});


// Design Element Cards

createIfNew(Card, {
    cardtype_id: 1,
    title: "Bell Schedule",
    description: "Missing description.",
    quote_text: "And therefore never send to know for whom the bell tolls; it tolls for thee.",
    quote_author: "John Donne"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Technology Infrastructure",
    description: "Missing description.",
    quote_text: "What's important is not just to develop the technology; it's to develop the processes.",
    quote_author: "Hal Abelson"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Classroom Design",
    description: "Missing description.",
    quote_text: "I have found that when I create an environment that is challenging or mysterious to me, it automatically becomes magical to my students.",
    quote_author: "Randi Stone"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Graduation Requirements",
    description: "Missing description.",
    quote_text: "Four credits required, including 1 unit in American History and 1/2 unit each in Participation in Government and Economics.",
    quote_author: "NYSEd Diploma Requirements"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Extracurricular Offerings",
    description: "Missing description.",
    quote_text: "When I was a teenager, I began to settle into school because I'd discovered the extracurricular activities that interested me: music and theatre.",
    quote_author: "Morgan Freeman"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Student Assessment",
    description: "Missing description.",
    quote_text: "We believe that the best forms of assessment are those that are useful to the learners.",
    quote_author: "Brennan & Resnick"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Professional Development",
    description: "Missing description.",
    quote_text: "A teacher's education doesn't stop once she earns her teaching certificate.",
    quote_author: "Denise Brown"
});
createIfNew(Card, {
    cardtype_id: 1,
    title: "Building Design",
    description: "Missing description.",
    quote_text: "We shape our buildings, and afterwards our buildings shape us.",
    quote_author: "Winston Churchill"
});


// Design Value Cards

/** Theory of Human Intelligence **/

createIfNew(Card, {
    cardtype_id: 2,
    title: "Tabula Rasa",
    description: "Missing description.",
    quote_text: "Let us then suppose the mind to be, as we say, white paper void of all characters, without any ideas. How comes it to be furnished?",
    quote_author: "John Locke",
});
createIfNew(Card, {
    cardtype_id: 2,
    title: "General Intelligence",
    description: "Missing description.",
    quote_text: "You are here.",
    quote_author: "",
});
createIfNew(Card, {
    cardtype_id: 2,
    title: "Theory of Recollection",
    description: "Missing description.",
    quote_text: "Do you see, Meno, what advances he has made in his power of recollection?",
    quote_author: "Socrates",
});
createIfNew(Card, {
    cardtype_id: 2,
    title: "Multiple Intelligences",
    description: "Missing description.",
    quote_text: "I believe that the brain has evolved over millions of years to be responsive to different kinds of content in the world. Language content, musical content, spatial content, numerical content, etc.",
    quote_author: "Howard Gardner",
});


/** Theory of Learning **/

createIfNew(Card, {
    cardtype_id: 3,
    title: "Social Constructivist",
    description: "Missing description.",
    quote_text: "Children have real understanding only of that which they invent themselves, and each time that we try to teach them something too quickly, we keep them from reinventing it themselves.",
    quote_author: "Jean Piaget",
});
createIfNew(Card, {
    cardtype_id: 3,
    title: "Construtionist",
    description: "Missing description.",
    quote_text: "Everything [can] be understood by being constructed.",
    quote_author: "Jean Piaget",
});
createIfNew(Card, {
    cardtype_id: 3,
    title: "Behaviorist",
    description: "Missing description.",
    quote_text: "Give me a child and I'll shape him into anything.",
    quote_author: "B. F. Skinner",
});
createIfNew(Card, {
    cardtype_id: 3,
    title: "Connectivist",
    description: "Missing description.",
    quote_text: "The content is a MacGuffin.",
    quote_author: "Stephen Downes",
});
createIfNew(Card, {
    cardtype_id: 3,
    title: "Situated Learning",
    description: "Missing description.",
    quote_text: "[Situated Learning] takes as its focus the relationship between learning and the social situation in which it occurs.",
    quote_author: "William F. Hanks",
});
createIfNew(Card, {
    cardtype_id: 3,
    title: "Cognitivist",
    description: "Missing description.",
    quote_text: "The rules of the universe that we think we know are buried deep in our processes of perception.",
    quote_author: "Gregory Bateson",
});


/** Teaching Approach **/

createIfNew(Card, {
    cardtype_id: 4,
    title: "Elite College Prep",
    description: "Missing description.",
    quote_text: "[Prep school] was to prevent rich boys from turning into playboys or pantywaists. It trained them not to be prosperous (that they would be prosperous was almost certain), but to be good and useful.",
    quote_author: "Nicholas Lemann",
});
createIfNew(Card, {
    cardtype_id: 4,
    title: "College and Career Readiness",
    description: "Missing description.",
    quote_text: "I have witnessed how education opens doors, and I know that when sound instruction takes place, students experience the joys of new-found knowledge and the ability to excel.",
    quote_author: "Daniel Akala",
});
createIfNew(Card, {
    cardtype_id: 4,
    title: "Assimilation",
    description: "Missing description.",
    quote_text: "Come to the Public Schools. Learn the Language of America. Prepare for American Citizenship.",
    quote_author: "1917 Cleveland board of Education Poster",
});
createIfNew(Card, {
    cardtype_id: 4,
    title: "Bulwark of Democracy",
    description: "Missing description.",
    quote_text: "Another object of the revisal is, to diffuse knowledge more generally through the mass of the people.",
    quote_author: "Thomas Jefferson",
});
createIfNew(Card, {
    cardtype_id: 4,
    title: "Life Adjustment & Purpose",
    description: "Missing description.",
    quote_text: "For education in the Century of the Child aims at nothing less than the production of individuality through the integration of experience.",
    quote_author: "Harold Rugg & Ann Shumaker",
});
createIfNew(Card, {
    cardtype_id: 4,
    title: "Standardized Test Performance",
    description: "Missing description.",
    quote_text: "If an unfriendly foreign power had attempted to impose on America the mediocre educational performance that exists today, we might well have viewed it as an act of war.",
    quote_author: "A Nation at Risk",
});


/** Purpose of Schooling **/

createIfNew(Card, {
    cardtype_id: 5,
    title: "Apprenticeship",
    description: "Missing description.",
    quote_text: "To find out what one is fitted to do, and to secure an opportunity to do it, is the key to happiness.",
    quote_author: "John Dewey",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "Project-Based Learning",
    description: "Missing description.",
    quote_text: "We actually learn a lot more than we would just sitting behind a desk with a book.",
    quote_author: "Student, Hurst Middle School, Destrehan, LA",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "Flipped Classroom",
    description: "Missing description.",
    quote_text: "The professor [as] sage on the stage...will not be effective for the 21st century, when individuals will be expected to think for themselves.",
    quote_author: "Allison King",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "Vocational Education",
    description: "Missing description.",
    quote_text: "You owe it to all of us to get on with what you're good at.",
    quote_author: "W.H. Auden",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "Mastery Learning & Standards-Based Grading",
    description: "Missing description.",
    quote_text: "We cannot control student socioeconomic levels, school funding...or a host of other important issues. However, we can control how we assess students.",
    quote_author: "Patricia Scriffiny",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "Design-Based Learning",
    description: "Missing description.",
    quote_text: "DBL provides a reason for learning science content by engaging the student in design and using a natural and meaningful venue.",
    quote_author: "Doppelt et al 2008",
});
createIfNew(Card, {
    cardtype_id: 5,
    title: "School of One",
    description: "Missing description.",
    quote_text: "We're looking in a way that I don't think anyone has looked at--at the way children learn, pacing them at their own pace, all of it tied to the mastery of content and skill and achievement.",
    quote_author: "Joel I. Klein",
});
